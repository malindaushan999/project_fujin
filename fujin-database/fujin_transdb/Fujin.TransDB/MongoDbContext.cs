﻿using Fujin.Common;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fujin.TransDB
{
    public class MongoDbContext
    {
        private readonly AppSettings _appSettings;
        private readonly MongoClient _dbClient;
        private readonly string _connString;

        public MongoDbContext(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
            _connString = APIUtility.CreateConnectionString(_appSettings.MongoDbConfig);
            _dbClient = new MongoClient(_connString);
        }

        public async Task<object> Test()
        {
            try
            {
                string dbName = Encoding.UTF8.GetString(Convert.FromBase64String(_appSettings.MongoDbConfig.Database));
                var database = _dbClient.GetDatabase(dbName);
                var collection = database.GetCollection<dynamic>("TestCollection");

                var filter = Builders<BsonDocument>.Filter.Eq("gender", "Female");
                var document = await collection.AggregateAsync<dynamic>("{\"gender\":\"Female\"}").ToList();

                return document;
            }
            catch (Exception ex)
            {
                Console.WriteLine("[EXCEPTION] {0}", ex.Message);
                return null;
            }
        }
    }
}
