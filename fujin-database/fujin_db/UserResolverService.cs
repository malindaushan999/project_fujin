﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fujin.DB
{
    public class UserResolverService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserResolverService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public m_user GetLoggedInUser()
        {
            m_user user = (m_user)_httpContextAccessor.HttpContext.Items["User"];
            return user;
        }

        public string GetLoggedInUsername()
        {
            m_user user = (m_user)_httpContextAccessor.HttpContext.Items["User"];
            return user?.username;
        }
    }
}
