﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Fujin.DB
{
    [Table("t_raw_data", Schema = "master")]
    [Index(nameof(sensor_id), Name = "IX_t_raw_data_sensor_id")]
    public partial class t_raw_data
    {
        [Key]
        public int device_id { get; set; }
        [Key]
        public int sensor_id { get; set; }
        [Key]
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime timestamp { get; set; }
        [Column(TypeName = "character varying")]
        public string data { get; set; }

        [ForeignKey(nameof(device_id))]
        [InverseProperty(nameof(m_device.t_raw_data))]
        public virtual m_device device { get; set; }
        [ForeignKey(nameof(sensor_id))]
        [InverseProperty(nameof(m_sensor.t_raw_data))]
        public virtual m_sensor sensor { get; set; }
    }
}
