﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Fujin.DB
{
    [Table("c_xxbt", Schema = "master")]
    public partial class c_xxbt
    {
        [Key]
        [StringLength(32)]
        public string identifier { get; set; }
        [Key]
        [StringLength(2)]
        public string div_value { get; set; }
        [Required]
        [StringLength(128)]
        public string label { get; set; }
    }
}
