﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Fujin.DB
{
    [Table("t_login_session", Schema = "master")]
    public partial class t_login_session
    {
        [Key]
        [StringLength(512)]
        public string session_id { get; set; }
        public int user_id { get; set; }
        public short is_active { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime logged_in_time { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime? logged_out_time { get; set; }
    }
}
