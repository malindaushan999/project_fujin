﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Fujin.DB
{
    public partial class FujinMasterContext
    {
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            SaveChangesInterceptor();
            return base.SaveChangesAsync(cancellationToken);
        }

        public override int SaveChanges()
        {
            SaveChangesInterceptor();
            return base.SaveChanges();
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            SaveChangesInterceptor();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            SaveChangesInterceptor();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        /// <summary>
        /// Intercepts SaveChanges() methods and attach extended data
        /// </summary>
        private void SaveChangesInterceptor()
        {
            // Get logged in user's username
            string username = this.GetService<UserResolverService>().GetLoggedInUsername();

            foreach (var entity in ChangeTracker.Entries())
            {
                if (!entity.Entity.GetType().Name.StartsWith("m_"))
                {
                    continue;
                }

                switch (entity.State)
                {
                    // Attach extended data for New entities
                    case EntityState.Added:
                        entity.CurrentValues["created_by"] = username;
                        entity.CurrentValues["created_timestamp"] = DateTime.UtcNow;
                        entity.CurrentValues["record_version_num"] = 0;
                        entity.CurrentValues["delete_flag"] = 0;
                        break;

                    // Attach extended data for Updated entities
                    case EntityState.Modified:
                        entity.CurrentValues["modified_by"] = username;
                        entity.CurrentValues["modified_timestamp"] = DateTime.UtcNow;
                        entity.CurrentValues["record_version_num"] = (int)entity.CurrentValues["record_version_num"] + 1;
                        break;

                    // Attach extended data for Deleted entities
                    case EntityState.Deleted:
                        entity.CurrentValues["deleted_by"] = username;
                        entity.CurrentValues["deleted_timestamp"] = DateTime.UtcNow;
                        entity.CurrentValues["record_version_num"] = (int)entity.CurrentValues["record_version_num"] + 1;
                        entity.CurrentValues["delete_flag"] = 1;

                        // Change Delete status to Modified status since the operation is SoftDelete
                        entity.State = EntityState.Modified;
                        break;
                }
            }
        }
    }
}
