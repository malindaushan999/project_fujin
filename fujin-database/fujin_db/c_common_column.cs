﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Fujin.DB
{
    [Keyless]
    [Table("c_common_column", Schema = "master")]
    public partial class c_common_column
    {
        [StringLength(32)]
        public string created_by { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime? created_timestamp { get; set; }
        [StringLength(32)]
        public string modified_by { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime? modified_timestamp { get; set; }
        [StringLength(32)]
        public string deleted_by { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime? deleted_timestamp { get; set; }
        public int delete_flag { get; set; }
        public int? record_version_num { get; set; }
    }
}
