﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Fujin.DB
{
    [Table("m_device_authority", Schema = "master")]
    [Index(nameof(device_id), Name = "IX_m_device_authority_device_id")]
    public partial class m_device_authority
    {
        [Key]
        public int user_id { get; set; }
        [Key]
        public int device_id { get; set; }
        [StringLength(2)]
        public string auth_level { get; set; }
        [StringLength(32)]
        public string created_by { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime? created_timestamp { get; set; }
        [StringLength(32)]
        public string modified_by { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime? modified_timestamp { get; set; }
        [StringLength(32)]
        public string deleted_by { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime? deleted_timestamp { get; set; }
        public int delete_flag { get; set; }
        public int? record_version_num { get; set; }

        [ForeignKey(nameof(device_id))]
        [InverseProperty(nameof(m_device.m_device_authority))]
        public virtual m_device device { get; set; }
        [ForeignKey(nameof(user_id))]
        [InverseProperty(nameof(m_user.m_device_authority))]
        public virtual m_user user { get; set; }
    }
}
