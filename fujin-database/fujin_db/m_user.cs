﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Fujin.DB
{
    [Table("m_user", Schema = "master")]
    public partial class m_user
    {
        public m_user()
        {
            m_device_authority = new HashSet<m_device_authority>();
        }

        [Key]
        public int user_id { get; set; }
        [Required]
        [StringLength(32)]
        public string username { get; set; }
        [Required]
        [StringLength(256)]
        [JsonIgnore]
        public string password { get; set; }
        [Required]
        [StringLength(64)]
        public string f_name { get; set; }
        [StringLength(64)]
        public string l_name { get; set; }
        [StringLength(256)]
        public string address { get; set; }
        [StringLength(20)]
        public string phone { get; set; }
        [Required]
        [StringLength(128)]
        public string email { get; set; }
        [StringLength(32)]
        public string created_by { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime? created_timestamp { get; set; }
        [StringLength(32)]
        public string modified_by { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime? modified_timestamp { get; set; }
        [StringLength(32)]
        public string deleted_by { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime? deleted_timestamp { get; set; }
        public int delete_flag { get; set; }
        public int? record_version_num { get; set; }

        [InverseProperty("user")]
        [JsonIgnore]
        public virtual ICollection<m_device_authority> m_device_authority { get; set; }
    }
}
