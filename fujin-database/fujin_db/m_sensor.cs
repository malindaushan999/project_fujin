﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Fujin.DB
{
    [Table("m_sensor", Schema = "master")]
    [Index(nameof(device_id), Name = "IX_m_sensor_device_id")]
    public partial class m_sensor
    {
        public m_sensor()
        {
            m_device_sensor_threshold = new HashSet<m_device_sensor_threshold>();
            t_raw_data = new HashSet<t_raw_data>();
        }

        [Key]
        public int sensor_id { get; set; }
        [Required]
        [StringLength(128)]
        public string name { get; set; }
        [StringLength(512)]
        public string description { get; set; }
        [StringLength(2)]
        public string status { get; set; }
        [StringLength(2)]
        public string unit { get; set; }
        public int? device_id { get; set; }
        [StringLength(32)]
        public string created_by { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime? created_timestamp { get; set; }
        [StringLength(32)]
        public string modified_by { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime? modified_timestamp { get; set; }
        [StringLength(32)]
        public string deleted_by { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime? deleted_timestamp { get; set; }
        public int delete_flag { get; set; }
        public int? record_version_num { get; set; }

        [ForeignKey(nameof(device_id))]
        [InverseProperty(nameof(m_device.m_sensor))]
        public virtual m_device device { get; set; }
        [InverseProperty("sensor")]
        public virtual ICollection<m_device_sensor_threshold> m_device_sensor_threshold { get; set; }
        [InverseProperty("sensor")]
        public virtual ICollection<t_raw_data> t_raw_data { get; set; }
    }
}
