﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Fujin.DB
{
    [Table("m_device_sensor_threshold", Schema = "master")]
    [Index(nameof(threshold_id), Name = "m_device_sensor_threshold_threshold_id_uindex", IsUnique = true)]
    public partial class m_device_sensor_threshold
    {
        [Key]
        public int device_id { get; set; }
        [Key]
        public int sensor_id { get; set; }
        [Key]
        public int threshold_id { get; set; }
        [StringLength(128)]
        public string display_name { get; set; }
        public decimal? threshold_value { get; set; }
        [StringLength(32)]
        public string created_by { get; set; }
        public DateTime? created_timestamp { get; set; }
        [StringLength(32)]
        public string modified_by { get; set; }
        public DateTime? modified_timestamp { get; set; }
        [StringLength(32)]
        public string deleted_by { get; set; }
        public DateTime? deleted_timestamp { get; set; }
        public int delete_flag { get; set; }
        public int record_version_num { get; set; }

        [ForeignKey(nameof(device_id))]
        [InverseProperty(nameof(m_device.m_device_sensor_threshold))]
        public virtual m_device device { get; set; }
        [ForeignKey(nameof(sensor_id))]
        [InverseProperty(nameof(m_sensor.m_device_sensor_threshold))]
        public virtual m_sensor sensor { get; set; }
    }
}
