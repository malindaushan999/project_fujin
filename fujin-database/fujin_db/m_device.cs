﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Fujin.DB
{
    [Table("m_device", Schema = "master")]
    public partial class m_device
    {
        public m_device()
        {
            m_device_authority = new HashSet<m_device_authority>();
            m_device_sensor_threshold = new HashSet<m_device_sensor_threshold>();
            m_sensor = new HashSet<m_sensor>();
            t_raw_data = new HashSet<t_raw_data>();
        }

        [Key]
        public int device_id { get; set; }
        [Required]
        [StringLength(32)]
        public string name { get; set; }
        [StringLength(2)]
        public string type { get; set; }
        [StringLength(128)]
        public string description { get; set; }
        [StringLength(64)]
        public string location { get; set; }
        public float? latitude { get; set; }
        public float? longitude { get; set; }
        [StringLength(2)]
        public string status { get; set; }
        [StringLength(2)]
        public string power_level { get; set; }
        [StringLength(256)]
        public string api_key { get; set; }
        [StringLength(32)]
        public string created_by { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime? created_timestamp { get; set; }
        [StringLength(32)]
        public string modified_by { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime? modified_timestamp { get; set; }
        [StringLength(32)]
        public string deleted_by { get; set; }
        [Column(TypeName = "timestamp(0) without time zone")]
        public DateTime? deleted_timestamp { get; set; }
        public int delete_flag { get; set; }
        public int? record_version_num { get; set; }

        [InverseProperty("device")]
        public virtual ICollection<m_device_authority> m_device_authority { get; set; }
        [InverseProperty("device")]
        public virtual ICollection<m_device_sensor_threshold> m_device_sensor_threshold { get; set; }
        [InverseProperty("device")]
        public virtual ICollection<m_sensor> m_sensor { get; set; }
        [InverseProperty("device")]
        public virtual ICollection<t_raw_data> t_raw_data { get; set; }
    }
}
