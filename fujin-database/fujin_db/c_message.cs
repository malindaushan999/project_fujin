﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Fujin.DB
{
    [Table("c_message", Schema = "master")]
    public partial class c_message
    {
        [Key]
        [StringLength(5)]
        public string code { get; set; }
        [Required]
        [Column(TypeName = "character varying")]
        public string message { get; set; }
        public int type { get; set; }
    }
}
