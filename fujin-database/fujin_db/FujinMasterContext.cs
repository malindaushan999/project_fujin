﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Fujin.DB
{
    public partial class FujinMasterContext : DbContext
    {
        public FujinMasterContext()
        {
        }

        public FujinMasterContext(DbContextOptions<FujinMasterContext> options)
            : base(options)
        {
        }

        public virtual DbSet<c_common_column> c_common_column { get; set; }
        public virtual DbSet<c_message> c_message { get; set; }
        public virtual DbSet<c_xxbt> c_xxbt { get; set; }
        public virtual DbSet<m_device> m_device { get; set; }
        public virtual DbSet<m_device_authority> m_device_authority { get; set; }
        public virtual DbSet<m_device_sensor_threshold> m_device_sensor_threshold { get; set; }
        public virtual DbSet<m_sensor> m_sensor { get; set; }
        public virtual DbSet<m_user> m_user { get; set; }
        public virtual DbSet<t_login_session> t_login_session { get; set; }
        public virtual DbSet<t_raw_data> t_raw_data { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseNpgsql("Host=54.238.67.15;Database=fujin_master;Username=postgres;Password=p@55w0rd");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "C.UTF-8");

            modelBuilder.Entity<c_message>(entity =>
            {
                entity.HasKey(e => e.code)
                    .HasName("c_message_pk");
            });

            modelBuilder.Entity<c_xxbt>(entity =>
            {
                entity.HasKey(e => new { e.identifier, e.div_value })
                    .HasName("c_xxbt_pk");
            });

            modelBuilder.Entity<m_device>(entity =>
            {
                entity.HasKey(e => e.device_id)
                    .HasName("m_device_pk");
            });

            modelBuilder.Entity<m_device_authority>(entity =>
            {
                entity.HasKey(e => new { e.user_id, e.device_id })
                    .HasName("m_device_authority_pk");

                entity.HasOne(d => d.device)
                    .WithMany(p => p.m_device_authority)
                    .HasForeignKey(d => d.device_id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("m_device_authority_fk_1");

                entity.HasOne(d => d.user)
                    .WithMany(p => p.m_device_authority)
                    .HasForeignKey(d => d.user_id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("m_device_authority_fk");
            });

            modelBuilder.Entity<m_device_sensor_threshold>(entity =>
            {
                entity.HasKey(e => new { e.device_id, e.sensor_id, e.threshold_id })
                    .HasName("m_device_sensor_threshold_pk");

                entity.Property(e => e.threshold_id).ValueGeneratedOnAdd();

                entity.HasOne(d => d.device)
                    .WithMany(p => p.m_device_sensor_threshold)
                    .HasForeignKey(d => d.device_id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("m_device_sensor_threshold_m_device_device_id_fk");

                entity.HasOne(d => d.sensor)
                    .WithMany(p => p.m_device_sensor_threshold)
                    .HasForeignKey(d => d.sensor_id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("m_device_sensor_threshold_m_sensor_sensor_id_fk");
            });

            modelBuilder.Entity<m_sensor>(entity =>
            {
                entity.HasKey(e => e.sensor_id)
                    .HasName("m_sensor_pk");

                entity.HasOne(d => d.device)
                    .WithMany(p => p.m_sensor)
                    .HasForeignKey(d => d.device_id)
                    .HasConstraintName("m_sensor_fk");
            });

            modelBuilder.Entity<m_user>(entity =>
            {
                entity.HasKey(e => e.user_id)
                    .HasName("m_user_pk");
            });

            modelBuilder.Entity<t_login_session>(entity =>
            {
                entity.HasKey(e => e.session_id)
                    .HasName("t_login_session_pk");
            });

            modelBuilder.Entity<t_raw_data>(entity =>
            {
                entity.HasKey(e => new { e.device_id, e.sensor_id, e.timestamp })
                    .HasName("d_raw_data_pk");

                entity.HasOne(d => d.device)
                    .WithMany(p => p.t_raw_data)
                    .HasForeignKey(d => d.device_id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("d_raw_data_fk");

                entity.HasOne(d => d.sensor)
                    .WithMany(p => p.t_raw_data)
                    .HasForeignKey(d => d.sensor_id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("d_raw_data_fk_1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
