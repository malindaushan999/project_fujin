// Use below scaffold command to create entities from database
dotnet ef dbcontext scaffold "Host=localhost;Database=fujin_master;Username=admin;Password=********" Npgsql.EntityFrameworkCore.PostgreSQL --use-database-names --no-pluralize -f -d -v -c FujinMasterContext

// AWS
dotnet ef dbcontext scaffold "Host=54.238.67.15;Database=fujin_master;Username=postgres;Password=********" Npgsql.EntityFrameworkCore.PostgreSQL --use-database-names --no-pluralize -f -d -v -c FujinMasterContext

// Set [JsonIgnore] to below fields

class => field
------------------------------
m_user => password
m_user => m_device_authority