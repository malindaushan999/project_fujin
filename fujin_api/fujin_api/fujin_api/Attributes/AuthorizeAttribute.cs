﻿using Fujin.API.Common;
using Fujin.DataContracts;
using Fujin.DataContracts.Base;
using Fujin.DB;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fujin.API.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            m_user user = (m_user)context.HttpContext.Items["User"];
            if (user == null)
            {
                var response = new BaseResponse()
                {
                    Code = ErrorCode.SYS_E10001,
                    Message = "Unauthorized",
                    Type = (int)MessageType.Error
                };

                context.Result = new JsonResult(response)
                {
                    StatusCode = StatusCodes.Status401Unauthorized
                };
            }
        }
    }
}
