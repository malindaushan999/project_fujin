﻿using Fujin.API.Common;
using Fujin.DataContracts;
using Fujin.DataContracts.Base;
using Fujin.DB;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fujin.API.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class DeviceAuthAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            m_device device = (m_device)context.HttpContext.Items["Device"];
            if (device == null)
            {
                var response = new BaseResponse()
                {
                    Code = ErrorCode.DEVICE_E12004,
                    Message = "Unrecognized device.",
                    Type = (int)MessageType.Error
                };

                context.Result = new JsonResult(response)
                {
                    StatusCode = StatusCodes.Status401Unauthorized
                };
            }
        }
    }
}
