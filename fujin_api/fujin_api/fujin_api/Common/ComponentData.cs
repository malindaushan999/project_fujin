﻿using Fujin.DataContracts;
using Fujin.DataContracts.Base;
using Fujin.DB;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fujin.API.Common
{
    public class ComponentData
    {
        private readonly IServiceScopeFactory _scopeFactory;

        public List<c_message> CMessage { get; set; } = new List<c_message>();
        public List<c_xxbt> CXXBT { get; set; } = new List<c_xxbt>();

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="scopeFactory">Scope factory</param>
        public ComponentData(IServiceScopeFactory scopeFactory)
        {
            _scopeFactory = scopeFactory;
            Initialize();
        }

        /// <summary>
        /// Initialize Component data
        /// </summary>
        private void Initialize()
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                using (var dbContext = scope.ServiceProvider.GetRequiredService<FujinMasterContext>())
                {
                    CMessage = dbContext.c_message.ToList();
                    CXXBT = dbContext.c_xxbt.ToList();
                }
            }
        }

        /// <summary>
        /// Get message by the code
        /// </summary>
        /// <param name="code">Message code</param>
        /// <returns>Error message</returns>
        public c_message GetMessageByCode(string code)
        {
            return CMessage.SingleOrDefault(x => x.code == code) ?? CMessage.SingleOrDefault(x => x.code == ErrorCode.SYS_E10002);
        }

        /// <summary>
        /// Get xxbt dropdown list 
        /// </summary>
        /// <param name="identifier">identifier</param>
        /// <returns>c_xxbt object list</returns>
        public List<c_xxbt> GetXXBTDropdownList(string identifier)
        {
            return CXXBT.Where(x => x.identifier == identifier).ToList();
        }
    }
}
