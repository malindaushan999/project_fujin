﻿using Fujin.API.Attributes;
using Fujin.API.Common;
using Fujin.API.Services;
using Fujin.DataContracts;
using Fujin.DataContracts.Base;
using Fujin.DataContracts.Device;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Fujin.DataContracts.Enums;

namespace Fujin.API.Controllers
{
    [Route("FujinAPI/[controller]")]
    [ApiController]
    public class DeviceController : ControllerBase
    {
        private IDeviceService _deviceService;
        private readonly ComponentData _componentData;
        private readonly ILogger<DeviceController> _logger;

        public DeviceController(IDeviceService deviceService, ComponentData componentData, ILogger<DeviceController> logger)
        {
            _deviceService = deviceService;
            _componentData = componentData;
            _logger = logger;
        }

        // POST: FujinAPI/CreateAPIKey
        [HttpPost("CreateAPIKey")]
        [Authorize]
        public async Task<IActionResult> CreateAPIKey([FromBody] DeviceAPIKeyRequest request)
        {
            var response = await _deviceService.CreateDeviceAPIKeyAsync(request);

            if (response == null || response.Code == ErrorCode.SYS_E10000)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.DEVICE_E12001);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return StatusCode(StatusCodes.Status206PartialContent, errorResponse);
            }

            if (response.Type == (int)ERROR_TYPE.ERROR)
            {
                return StatusCode(StatusCodes.Status206PartialContent, response);
            }

            return Ok(response);
        }

        // POST: FujinAPI/UploadData
        [HttpPost("UploadData")]
        [DeviceAuth]
        public async Task<IActionResult> UploadData([FromHeader(Name = "x-device-auth")] string authKey, [FromBody]List<DataUploadEntity> rawdata)
        {
            var response = await _deviceService.UploadDataAsync(rawdata);

            if (response == null || response.Code == ErrorCode.SYS_E10000)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.DEVICE_E12003);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return StatusCode(StatusCodes.Status206PartialContent, errorResponse);
            }

            if (response.Type == (int)ERROR_TYPE.ERROR)
            {
                return StatusCode(StatusCodes.Status206PartialContent, response);
            }

            return Ok(response);
        }

        // POST: FujinAPI/RetrievLatestData
        [HttpPost("RetrievLatestData")]
        [DeviceAuth]
        public async Task<IActionResult> RetrievLatestData([FromHeader(Name = "x-device-auth")] string authKey, [FromBody]RetrievDataRequest req)
        {
            if (req.TargetDeviceID <= 0 || !_deviceService.IsValidDevice(req.TargetDeviceID))
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.DEVICE_E12007);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return StatusCode(StatusCodes.Status206PartialContent, errorResponse);
            }

            if (req.TargetSensorID != 0 && (req.TargetSensorID < 0 || !_deviceService.IsValidSensor(req.TargetSensorID)))
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.DEVICE_E12008);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return StatusCode(StatusCodes.Status206PartialContent, errorResponse);
            }

            var response = await _deviceService.RetrievLatestData(req);

            if (response == null || response.Code == ErrorCode.SYS_E10000)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.DEVICE_E12006);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return StatusCode(StatusCodes.Status206PartialContent, errorResponse);
            }

            if (response.Type == (int)ERROR_TYPE.ERROR)
            {
                return StatusCode(StatusCodes.Status206PartialContent, response);
            }

            return Ok(response);
        }
    }
}
