﻿using Fujin.API.Attributes;
using Fujin.API.Common;
using Fujin.API.Services;
using Fujin.Common;
using Fujin.DataContracts;
using Fujin.DataContracts.Base;
using Fujin.DataContracts.User;
using Fujin.DB;
using Fujin.TransDB;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Fujin.DataContracts.Enums;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Fujin.API.Controllers
{
    [Route("FujinAPI/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserService _userService;
        private readonly ComponentData _componentData;
        private readonly ILogger<UserController> _logger;
        private readonly MongoDbContext _mongoDbContext;

        public UserController(IUserService userService, IOptions<AppSettings> appSettings, ComponentData componentData, ILogger<UserController> logger, MongoDbContext mongoDbContext)
        {
            _userService = userService;
            _componentData = componentData;
            _logger = logger;
            _mongoDbContext = mongoDbContext;
        }


        [HttpGet("Test")]
        public async Task<IActionResult> Test()
        {
            var res = await _mongoDbContext.Test();
            return Ok(res);
        }

        // POST: FujinAPI/Authenticate
        [HttpPost("Authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] AuthRequest request)
        {
            var response = await _userService.Authenticate(request);

            if (response == null || response.Code == ErrorCode.SYS_E10000)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11002);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return Unauthorized(errorResponse);
            }

            if (response.Type == (int)ERROR_TYPE.ERROR)
            {
                return Unauthorized(response);
            }

            return Ok(response);
        }

        // GET: FujinAPI/Logout
        [HttpGet("Logout")]
        [Authorize]
        public async Task<IActionResult> Logout()
        {
            var response = await _userService.Logout();

            if (response == null || response.Code == ErrorCode.SYS_E10000)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11003);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return StatusCode(StatusCodes.Status206PartialContent, errorResponse);
            }

            if (response.Type == (int)ERROR_TYPE.ERROR)
            {
                return StatusCode(StatusCodes.Status206PartialContent, response);
            }

            return Ok(response);
        }

        // GET: FujinAPI/GetUser
        [HttpGet("GetUser")]
        [Authorize]
        public async Task<IActionResult> GetUser([FromQuery] int userID)
        {
            var response = await _userService.GetUserAsync(userID);

            if (response == null || response.Code == ErrorCode.SYS_E10000)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11019);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return StatusCode(StatusCodes.Status206PartialContent, errorResponse);
            }

            if (response.Code == ErrorCode.USER_E11012)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11012);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return StatusCode(StatusCodes.Status206PartialContent, errorResponse);
            }

            return Ok(response);
        }

        // GET: FujinAPI/UserList
        [HttpGet("UserList")]
        [Authorize]
        public async Task<IActionResult> UserList()
        {
            var response = await _userService.GetAllUsersAsync();

            if (response == null || response.Code == ErrorCode.SYS_E10000)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11006);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return StatusCode(StatusCodes.Status206PartialContent, errorResponse);
            }

            return Ok(response);
        }

        // POST: FujinAPI/CreateNewUser
        [HttpPost("CreateNewUser")]
        [Authorize]
        public async Task<IActionResult> CreateNewUser([FromBody] UserUpsertRequest request)
        {
            // Validate request
            if (!ValidateUserData(request))
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11013);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return StatusCode(StatusCodes.Status206PartialContent, errorResponse);
            }

            // Process request
            var response = await _userService.UpsertAsync(UPSERT_MODE.INSERT, request);

            // Handle Response
            if (response == null || response.Code == ErrorCode.SYS_E10000)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11008);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return StatusCode(StatusCodes.Status206PartialContent, errorResponse);
            }

            if (response.Type == (int)ERROR_TYPE.ERROR)
            {
                return StatusCode(StatusCodes.Status206PartialContent, response);
            }

            return Ok(response);
        }

        // POST: FujinAPI/EditUser
        [HttpPost("EditUser")]
        [Authorize]
        public async Task<IActionResult> EditUser([FromBody] UserUpsertRequest request)
        {
            // Validate request
            if (!ValidateUserData(request))
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11013);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return StatusCode(StatusCodes.Status206PartialContent, errorResponse);
            }

            // Process request
            var response = await _userService.UpsertAsync(UPSERT_MODE.UPDATE, request);

            // Handle Response
            if (response == null || response.Code == ErrorCode.SYS_E10000)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11008);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return StatusCode(StatusCodes.Status206PartialContent, errorResponse);
            }

            if (response.Type == (int)ERROR_TYPE.ERROR)
            {
                return StatusCode(StatusCodes.Status206PartialContent, response);
            }

            return Ok(response);
        }

        // DELETE: FujinAPI/DeleteUser
        [HttpDelete("DeleteUser")]
        [Authorize]
        public async Task<IActionResult> DeleteUser(int userid)
        {
            // Process request
            var response = await _userService.DeleteAsync(userid);

            // Handle Response
            if (response == null || response.Code == ErrorCode.SYS_E10000)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11015);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return StatusCode(StatusCodes.Status206PartialContent, errorResponse);
            }

            if (response.Type == (int)ERROR_TYPE.ERROR)
            {
                return StatusCode(StatusCodes.Status206PartialContent, response);
            }

            return Ok(response);
        }

        // GET: FujinAPI/ResetPassword
        [HttpGet("ResetPassword")]
        [Authorize]
        public async Task<IActionResult> ResetPassword([FromQuery] int userID)
        {
            var response = await _userService.ResetPasswordAsync(userID);

            if (response == null || response.Code == ErrorCode.SYS_E10000)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11019);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return StatusCode(StatusCodes.Status206PartialContent, errorResponse);
            }

            if (response.Code == ErrorCode.USER_E11012)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11012);
                BaseResponse errorResponse = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                return StatusCode(StatusCodes.Status206PartialContent, errorResponse);
            }

            return Ok(response);
        }

        /// <summary>
        /// Validate the user data of the incoming request before processing
        /// </summary>
        /// <param name="request">Incoming request</param>
        /// <returns>Validation Result</returns>
        private bool ValidateUserData(UserUpsertRequest request)
        {
            if (request.UserID < 0) return false;
            if (string.IsNullOrEmpty(request.Username) || request.Username.Length > 32) return false;
            if (string.IsNullOrEmpty(request.FirstName) || request.FirstName.Length > 64) return false;
            if (request.LastName.Length > 64) return false;
            if (request.Address.Length > 256) return false;
            if (request.Phone.Length > 20) return false;
            if (string.IsNullOrEmpty(request.Email) || request.Email.Length > 128) return false;
            return true;
        }
    }
}
