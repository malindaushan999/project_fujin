﻿using Fujin.API.Common;
using Fujin.API.Services;
using Fujin.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fujin.API.Middleware
{
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly AppSettings _appSettings;

        public AuthenticationMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings)
        {
            _next = next;
            _appSettings = appSettings.Value;
        }

        public async Task Invoke(HttpContext context, IUserService userService, IDeviceService deviceService)
        {
            try
            {
                string token = context.Request.Headers["Authorization"]
                .FirstOrDefault()?
                .Split(" ")
                .Last();

                if (!string.IsNullOrEmpty(token))
                {

                    JwtSecurityToken jwtToken = ValidateToken(token);
                    var userId = int.Parse(jwtToken.Claims.First(x => x.Type == "id").Value);

                    if (userService.GetUserStatus(token))
                    {
                        context.Items["User"] = userService.GetUserById(userId);
                    }
                }

                token = context.Request.Headers["x-device-auth"]
                    .FirstOrDefault()?
                    .Split(" ")
                    .Last();

                if (!string.IsNullOrEmpty(token))
                {
                    JwtSecurityToken jwtToken = ValidateToken(token);

                    if (true)
                    {
                        var deviceID = int.Parse(jwtToken.Claims.First(x => x.Type == "device_id").Value);
                        context.Items["Device"] = deviceService.GetDeviceById(deviceID); 
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("[EXCEPTION] => " + ex.Message);
            }

            await _next(context);
        }

        /// <summary>
        /// Validate token
        /// </summary>
        /// <param name="token">token</param>
        /// <returns>validated token</returns>
        private JwtSecurityToken ValidateToken(string token)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            byte[] key = Encoding.ASCII.GetBytes(_appSettings.Secret);

            tokenHandler.ValidateToken(token, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero
            }, out SecurityToken validatedToken);

            return (JwtSecurityToken)validatedToken;
        }
    }
}
