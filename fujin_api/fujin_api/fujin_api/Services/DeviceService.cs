﻿using Fujin.API.Common;
using Fujin.Common;
using Fujin.DataContracts;
using Fujin.DataContracts.Base;
using Fujin.DataContracts.Device;
using Fujin.DB;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Fujin.DataContracts.Enums;

namespace Fujin.API.Services
{
    public class DeviceService : IDeviceService
    {
        private readonly string _secrect;
        private readonly FujinMasterContext _dbContext;
        private readonly ComponentData _componentData;
        private readonly ILogger<DeviceService> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public DeviceService(IOptions<AppSettings> appSettings, FujinMasterContext dbContext, ComponentData componentData,
            ILogger<DeviceService> logger, IHttpContextAccessor httpContextAccessor)
        {
            _secrect = appSettings?.Value.Secret;
            _dbContext = dbContext;
            _componentData = componentData;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<DeviceAPIKeyResponse> CreateDeviceAPIKeyAsync(DeviceAPIKeyRequest req)
        {
            try
            {
                _logger.LogDebug("APIKey Creation Task Start");

                using (_dbContext)
                {
                    m_device device = await _dbContext.m_device.Where(x => x.delete_flag == (int)DELETE_FLAG.NOT_DELETE)
                        .Where(x => x.device_id == req.DeviceID)
                        .FirstOrDefaultAsync();

                    if (device == null)
                    {
                        return null;
                    }

                    // Generate APIKey
                    string APIKey = APIUtility.GenerateJwtToken(_secrect, device);
                    device.api_key = APIKey;

                    // Save changes to db
                    _dbContext.SaveChanges();

                    // Create response
                    c_message cMessage = _componentData.GetMessageByCode(ErrorCode.DEVICE_S12000);
                    DeviceAPIKeyResponse res = new()
                    {
                        Code = cMessage.code,
                        Message = cMessage.message,
                        Type = cMessage.type,
                        APIKey = APIKey
                    };

                    _logger.LogInformation(res.Message);

                    return res;
                }
            }
            catch (Exception ex)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.SYS_E10000);
                DeviceAPIKeyResponse res = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };

                _logger.LogError(cMessage.message);
                _logger.LogTrace(ex.Message);

                return res;
            }
            finally
            {
                _logger.LogDebug("APIKey Creation Task End");
            }
        }

        /// <summary>
        /// Upload data
        /// </summary>
        /// <param name="rawdata">data</param>
        /// <returns></returns>
        public async Task<BaseResponse> UploadDataAsync(List<DataUploadEntity> dataList)
        {
            try
            {
                _logger.LogDebug("Upload Data Task Start");

                if (dataList == null || dataList.Count == 0)
                {
                    return null;
                }

                // Check device availablility
                m_device device = (m_device)_httpContextAccessor.HttpContext.Items["Device"];
                if (device == null)
                {
                    return null;
                }

                using (_dbContext)
                {
                    foreach (var item in dataList)
                    {
                        // Create DB entity
                        t_raw_data rawdataEntity = new()
                        {
                            device_id = device.device_id,
                            sensor_id = item.SensorID,
                            timestamp = DateTime.UtcNow,
                            data = item.DataValue
                        };

                        // Add to table
                        await _dbContext.t_raw_data.AddAsync(rawdataEntity);
                    }

                    // Save changes to db
                    await _dbContext.SaveChangesAsync();

                    // Create response
                    c_message cMessage = _componentData.GetMessageByCode(ErrorCode.DEVICE_S12002);
                    BaseResponse res = new()
                    {
                        Code = cMessage.code,
                        Message = cMessage.message,
                        Type = cMessage.type
                    };

                    _logger.LogInformation(res.Message);

                    return res;
                }
            }
            catch (JsonException ex)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.SYS_E10003);
                BaseResponse res = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };

                _logger.LogError(cMessage.message);
                _logger.LogTrace(ex.Message);

                return res;
            }
            catch (Exception ex)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.SYS_E10000);
                BaseResponse res = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };

                _logger.LogError(cMessage.message);
                _logger.LogTrace(ex.Message);

                return res;
            }
            finally
            {
                _logger.LogDebug("Upload Data Task End");
            }
        }

        /// <summary>
        /// Retrive latest data
        /// </summary>
        /// <param name="req">Request params</param>
        /// <returns></returns>
        public async Task<RetrievDataResponse> RetrievLatestData(RetrievDataRequest req)
        {
            try
            {
                _logger.LogDebug("Retriev Latest Data Task Start");

                using (_dbContext)
                {
                    var iqList = _dbContext.t_raw_data.Where(x => x.device_id == req.TargetDeviceID)
                        .AsEnumerable()
                        .GroupBy(x => x.sensor_id)
                        .Select(x => new 
                        { 
                            sensor_id = x.Key,
                            data = x.OrderByDescending(o => o.timestamp).FirstOrDefault().data,
                            timestamp = x.OrderByDescending(o => o.timestamp).FirstOrDefault().timestamp
                        });

                    // Select All sensors of the selected device
                    if (req.TargetSensorID != 0)
                    {
                        iqList = iqList.Where(x => x.sensor_id == req.TargetSensorID);
                    }

                    List<ResDataSensor> resDataList = new();

                    foreach (var rawData in iqList.ToList())
                    {
                        if (rawData == null)
                        {
                            continue;
                        }
                        double.TryParse(rawData.data, out double dataValue);

                        ResDataSensor resData = new()
                        {
                            SensorID = rawData.sensor_id,
                            DataValue = dataValue,
                            Timestamp = rawData.timestamp
                        };

                        // Get threshold data
                        List<KeyValuePair<int, double>> thresholdKVList = new();

                        var thresholdList = await _dbContext.m_device_sensor_threshold.Where(x => x.delete_flag == (short)DELETE_FLAG.NOT_DELETE)
                            .Where(x => x.device_id == req.TargetDeviceID)
                            .Where(x => x.sensor_id == rawData.sensor_id)
                            .ToListAsync();

                        foreach (var item in thresholdList)
                        {
                            double thresholdValue = decimal.ToDouble(item.threshold_value ?? 0);
                            thresholdKVList.Add(new KeyValuePair<int, double>(item.threshold_id, thresholdValue));
                        }

                        resData.ThresholdList = thresholdKVList;

                        resDataList.Add(resData);
                    }

                    // If data is not found
                    if (resDataList.Count == 0)
                    {
                        return null;
                    }

                    // Create response
                    c_message cMessage = _componentData.GetMessageByCode(ErrorCode.DEVICE_S12005);
                    RetrievDataResponse res = new()
                    {
                        Code = cMessage.code,
                        Message = cMessage.message,
                        Type = cMessage.type,
                        DataList = resDataList
                    };

                    _logger.LogInformation(res.Message);

                    return res;
                }
            }
            catch (Exception ex)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.SYS_E10000);
                RetrievDataResponse res = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };

                _logger.LogError(cMessage.message);
                _logger.LogTrace(ex.Message);

                return res;
            }
            finally
            {
                _logger.LogDebug("Retriev Latest Data Task End");
            }
        }

        /// <summary>
        /// Get device
        /// </summary>
        /// <param name="id">Device ID</param>
        /// <returns></returns>
        public m_device GetDeviceById(int id)
        {
            try
            {
                return _dbContext.m_device.SingleOrDefault(x => x.device_id == id && x.delete_flag == (int)DELETE_FLAG.NOT_DELETE);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Is valid device
        /// </summary>
        /// <param name="id">Device ID</param>
        /// <returns>
        ///     True: Device is valid
        ///     False: Device is not valid
        /// </returns>
        public bool IsValidDevice(int id)
        {
            try
            {
                return _dbContext.m_device.FirstOrDefault(x => x.delete_flag == (short)DELETE_FLAG.NOT_DELETE && x.device_id == id) != null;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Is valid sensor
        /// </summary>
        /// <param name="id">Sensor ID</param>
        /// <returns>
        ///     True: Sensor is valid
        ///     False: Sensor is not valid
        /// </returns>
        public bool IsValidSensor(int id)
        {
            try
            {
                return _dbContext.m_sensor.FirstOrDefault(x => x.delete_flag == (short)DELETE_FLAG.NOT_DELETE && x.sensor_id == id) != null;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
