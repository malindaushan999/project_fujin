﻿using Fujin.API.Common;
using Fujin.Common;
using Fujin.DataContracts;
using Fujin.DataContracts.Base;
using Fujin.DataContracts.User;
using Fujin.DB;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Fujin.DataContracts.Enums;

namespace Fujin.API.Services
{
    public class UserService : IUserService
    {
        private readonly string _secrect;
        private readonly FujinMasterContext _dbContext;
        private readonly ComponentData _componentData;
        private readonly ILogger<UserService> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserService(IOptions<AppSettings> appSettings, FujinMasterContext dbContext, ComponentData componentData,
            ILogger<UserService> logger, IHttpContextAccessor httpContextAccessor)
        {
            _secrect = appSettings?.Value.Secret;
            _dbContext = dbContext;
            _componentData = componentData;
            _logger = logger;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<AuthResponse> Authenticate(AuthRequest req)
        {
            try
            {
                _logger.LogDebug("Authenticate Start");

                using (_dbContext)
                {
                    // Validate user
                    m_user user = _dbContext.m_user.SingleOrDefault(x => x.username == req.Username);

                    if (user == null)
                    {
                        return null;
                    }

                    // Match passwords
                    string reqPassword = Encoding.UTF8.GetString(Convert.FromBase64String(req.Password));
                    string dbPassword = Encoding.UTF8.GetString(Convert.FromBase64String(user.password));

                    if (reqPassword != dbPassword)
                    {
                        return null;
                    }

                    var token = APIUtility.GenerateJwtToken(_secrect, user);

                    // If a record already in 't_login_session', set 'is_active' to 0 (INACTIVE)
                    int oldSessionCount = _dbContext.t_login_session.Where(x => x.user_id == user.user_id)
                        .Where(x => x.is_active == (short)ACTIVE_FLAG.ACTIVE)
                        .Count();

                    if (oldSessionCount > 0)
                    {
                        List<t_login_session> loginSessionList = _dbContext.t_login_session.Where(x => x.user_id == user.user_id)
                            .Where(x => x.is_active == (short)ACTIVE_FLAG.ACTIVE)
                            .ToList();

                        foreach (t_login_session loginSession in loginSessionList)
                        {
                            loginSession.is_active = (short)ACTIVE_FLAG.INACTIVE;
                            loginSession.logged_out_time = DateTime.UtcNow;
                        }
                    }

                    // Add record to t_login_session table
                    t_login_session session = new()
                    {
                        session_id = token,
                        logged_in_time = DateTime.UtcNow,
                        user_id = user.user_id,
                        is_active = (short)ACTIVE_FLAG.ACTIVE
                    };
                    _dbContext.t_login_session.Add(session);

                    // Save changes to db
                    await _dbContext.SaveChangesAsync();

                    // Create response
                    var cMessage = _componentData.GetMessageByCode(ErrorCode.USER_S11000);
                    AuthResponse res = new()
                    {
                        AuthToken = token,
                        UserInfo = user,
                        Code = cMessage.code,
                        Message = cMessage.message,
                        Type = cMessage.type
                    };

                    _logger.LogInformation(cMessage.message);

                    return res;
                }
            }
            catch (Exception ex)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.SYS_E10000);
                AuthResponse res = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };

                _logger.LogError(cMessage.message);
                _logger.LogTrace(ex.Message);

                return res;
            }
            finally
            {
                _logger.LogDebug("Authenticate End");
            }
        }

        /// <summary>
        /// Get all not deleted users
        /// </summary>
        /// <returns>UserListResponse</returns>
        public async Task<UserListResponse> GetAllUsersAsync()
        {
            try
            {
                _logger.LogDebug("GetAllUsersAsync Start");

                var userList = _dbContext.m_user.Where(x => x.delete_flag == (short)ACTIVE_FLAG.ACTIVE).ToList();

                // Create response
                var cMessage = _componentData.GetMessageByCode(ErrorCode.USER_S11005);
                UserListResponse response = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type,
                    UserList = userList
                };

                _logger.LogInformation(cMessage.message);

                return response;
            }
            catch (Exception ex)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.SYS_E10000);
                _logger.LogError(cMessage.message);
                _logger.LogTrace(ex.Message);
                return null;
            }
            finally
            {
                _logger.LogDebug("GetAllUsersAsync End");
            }
        }

        public bool GetUserStatus(string sessionId)
        {
            try
            {
                var isActive = _dbContext.t_login_session.Where(x => x.session_id == sessionId).FirstOrDefault()?.is_active;
                if (isActive == (short)ACTIVE_FLAG.ACTIVE)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        public m_user GetUserById(int id)
        {
            try
            {
                return _dbContext.m_user.SingleOrDefault(x => x.user_id == id && x.delete_flag == (int)DELETE_FLAG.NOT_DELETE);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// GetUser
        /// </summary>
        /// <param name="id">User ID</param>
        /// <returns></returns>
        public async Task<UserResponse> GetUserAsync(int id)
        {
            try
            {
                UserResponse res = new();
                c_message cMessage = null;

                var user = GetUserById(id);

                if (user == null)
                {
                    cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11012);
                    res = new UserResponse()
                    {
                        Code = cMessage.code,
                        Message = cMessage.message,
                        Type = cMessage.type
                    };

                    _logger.LogInformation(cMessage.message);
                    return res;
                }

                cMessage = _componentData.GetMessageByCode(ErrorCode.USER_S11001);
                res = new UserResponse()
                {
                    User = user,
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };
                _logger.LogInformation(cMessage.message);

                return res;
            }
            catch (Exception ex)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.SYS_E10000);

                _logger.LogError(cMessage.message);
                _logger.LogTrace(ex.Message);

                return null;
            }
            finally
            {
                _logger.LogDebug("GetUser End");
            }
        }

        public async Task<BaseResponse> Logout()
        {
            try
            {
                _logger.LogDebug("Logout Start");

                BaseResponse res = new();

                m_user user = (m_user)_httpContextAccessor.HttpContext.Items["User"];
                if (user is not null)
                {
                    using (_dbContext)
                    {
                        List<t_login_session> loginSessionList = _dbContext.t_login_session.Where(x => x.user_id == user.user_id).ToList();
                        loginSessionList.ForEach(x => x.is_active = (short)ACTIVE_FLAG.INACTIVE);
                        loginSessionList.ForEach(x => x.logged_out_time = DateTime.UtcNow);

                        // Save changes to db
                        await _dbContext.SaveChangesAsync();

                        c_message cMessage = _componentData.GetMessageByCode(ErrorCode.USER_S11004);
                        res.Code = cMessage.code;
                        res.Message = cMessage.message;
                        res.Type = cMessage.type;
                    }
                }
                else
                {
                    c_message cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11003);
                    res.Code = cMessage.code;
                    res.Message = cMessage.message;
                    res.Type = cMessage.type;
                }

                return res;
            }
            catch (Exception ex)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.SYS_E10000);
                BaseResponse res = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };

                _logger.LogError(cMessage.message);
                _logger.LogTrace(ex.Message);

                return res;
            }
            finally
            {
                _logger.LogDebug("Logout End");
            }
        }

        /// <summary>
        /// Insert or Update User
        /// </summary>
        /// <param name="upsertMode">UPSERT_MODE</param>
        /// <returns></returns>
        public async Task<BaseResponse> UpsertAsync(UPSERT_MODE upsertMode, UserUpsertRequest req)
        {
            try
            {
                _logger.LogDebug("UpsertAsync Start");
                BaseResponse res = new();
                c_message cMessage = null;
                var dbUserIQ = _dbContext.m_user.Where(x => x.delete_flag == (short)DELETE_FLAG.NOT_DELETE);

                m_user user = (m_user)_httpContextAccessor.HttpContext.Items["User"];

                switch (upsertMode)
                {
                    case UPSERT_MODE.INSERT:
                        #region INSERT
                        // check username exists in the db
                        var dbUser = dbUserIQ.Where(x => x.username == req.Username).FirstOrDefault();
                        if (dbUser != null)
                        {
                            cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11011);
                            res = new()
                            {
                                Code = cMessage.code,
                                Message = cMessage.message,
                                Type = cMessage.type
                            };

                            return res;
                        }

                        // create new user entity
                        m_user mUser = new()
                        {
                            username = req.Username,
                            password = APIUtility.Base64Encode(req.Username),
                            f_name = req.FirstName,
                            l_name = req.LastName,
                            address = req.Address,
                            phone = req.Phone,
                            email = req.Email
                        };

                        await _dbContext.m_user.AddAsync(mUser);

                        // Get success message
                        cMessage = _componentData.GetMessageByCode(ErrorCode.USER_S11007);
                        #endregion INSERT
                        break;

                    case UPSERT_MODE.UPDATE:
                        #region UPDATE
                        // check the user is sysadmin since it can't be edited
                        if (req.UserID == StaticData.SYS_USERID || req.Username == StaticData.SYS_USERNAME)
                        {
                            cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11017);
                            res = new()
                            {
                                Code = cMessage.code,
                                Message = cMessage.message,
                                Type = cMessage.type
                            };

                            return res;
                        }

                        // check user exists in the db
                        dbUser = dbUserIQ.Where(x => x.user_id == req.UserID).FirstOrDefault();
                        if (dbUser == null)
                        {
                            cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11012);
                            res = new()
                            {
                                Code = cMessage.code,
                                Message = cMessage.message,
                                Type = cMessage.type
                            };

                            return res;
                        }

                        // check other user's exists with the same username other than the user that is being edited.
                        var sameDbUser = dbUserIQ.Where(x => x.username == req.Username && x != dbUser).FirstOrDefault();
                        if (sameDbUser != null)
                        {
                            cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11011);
                            res = new()
                            {
                                Code = cMessage.code,
                                Message = cMessage.message,
                                Type = cMessage.type
                            };

                            return res;
                        }

                        // set data values
                        dbUser.username = req.Username;
                        dbUser.f_name = req.FirstName;
                        dbUser.email = req.Email;
                        if (!string.IsNullOrEmpty(req.LastName)) dbUser.l_name = req.LastName;
                        if (!string.IsNullOrEmpty(req.Address)) dbUser.address = req.Address;
                        if (!string.IsNullOrEmpty(req.Phone)) dbUser.phone = req.Phone;

                        // Get success message
                        cMessage = _componentData.GetMessageByCode(ErrorCode.USER_S11009);
                        #endregion UPDATE
                        break;
                }

                await _dbContext.SaveChangesAsync();

                // create success response
                res = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };

                return res;
            }
            catch (Exception ex)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.SYS_E10000);

                _logger.LogError(cMessage.message);
                _logger.LogTrace(ex.Message);

                return null;
            }
            finally
            {
                _logger.LogDebug("UpsertAsync End");
            }
        }

        public async Task<BaseResponse> DeleteAsync(int userid)
        {
            try
            {
                _logger.LogDebug("DeleteAsync Start");
                BaseResponse res = new();
                c_message cMessage = null;
                var dbUserIQ = _dbContext.m_user.Where(x => x.delete_flag == (short)DELETE_FLAG.NOT_DELETE);
                var dbUser = dbUserIQ.Where(x => x.user_id == userid).FirstOrDefault();

                // check the user is sysadmin since it can't be deleted
                if (userid == StaticData.SYS_USERID)
                {
                    cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11018);
                    res = new()
                    {
                        Code = cMessage.code,
                        Message = cMessage.message,
                        Type = cMessage.type
                    };

                    return res;
                }

                // check user exists in the db
                if (dbUser == null)
                {
                    cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11012);
                    res = new()
                    {
                        Code = cMessage.code,
                        Message = cMessage.message,
                        Type = cMessage.type
                    };

                    return res;
                }

                // check own user delete case
                m_user user = (m_user)_httpContextAccessor.HttpContext.Items["User"];
                if (dbUser.user_id == user.user_id)
                {
                    cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11016);
                    res = new()
                    {
                        Code = cMessage.code,
                        Message = cMessage.message,
                        Type = cMessage.type
                    };

                    return res;
                }

                _dbContext.Remove(dbUser);
                await _dbContext.SaveChangesAsync();

                cMessage = _componentData.GetMessageByCode(ErrorCode.USER_S11014);
                res = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };

                return res;
            }
            catch (Exception ex)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.SYS_E10000);

                _logger.LogError(cMessage.message);
                _logger.LogTrace(ex.Message);

                return null;
            }
            finally
            {
                _logger.LogDebug("DeleteAsync End");
            }
        }

        /// <summary>
        /// ResetPassword
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public async Task<BaseResponse> ResetPasswordAsync(int userid)
        {
            try
            {
                _logger.LogDebug("ResetPasswordAsync Start");
                BaseResponse res = new();
                c_message cMessage = null;
                var dbUserIQ = _dbContext.m_user.Where(x => x.delete_flag == (short)DELETE_FLAG.NOT_DELETE);
                var dbUser = dbUserIQ.Where(x => x.user_id == userid).FirstOrDefault();

                // check user exists in the db
                if (dbUser == null)
                {
                    cMessage = _componentData.GetMessageByCode(ErrorCode.USER_E11012);
                    res = new()
                    {
                        Code = cMessage.code,
                        Message = cMessage.message,
                        Type = cMessage.type
                    };

                    return res;
                }

                dbUser.password = APIUtility.Base64Encode(dbUser.username);
                await _dbContext.SaveChangesAsync();

                cMessage = _componentData.GetMessageByCode(ErrorCode.USER_S11020);
                res = new()
                {
                    Code = cMessage.code,
                    Message = cMessage.message,
                    Type = cMessage.type
                };

                return res;
            }
            catch (Exception ex)
            {
                var cMessage = _componentData.GetMessageByCode(ErrorCode.SYS_E10000);

                _logger.LogError(cMessage.message);
                _logger.LogTrace(ex.Message);

                return null;
            }
            finally
            {
                _logger.LogDebug("ResetPasswordAsync End");
            }
        }
    }
}
