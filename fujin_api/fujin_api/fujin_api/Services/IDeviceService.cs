﻿using Fujin.DataContracts.Base;
using Fujin.DataContracts.Device;
using Fujin.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fujin.API.Services
{
    public interface IDeviceService
    {
        Task<DeviceAPIKeyResponse> CreateDeviceAPIKeyAsync(DeviceAPIKeyRequest req);
        Task<BaseResponse> UploadDataAsync(List<DataUploadEntity> dataList);
        m_device GetDeviceById(int id);
        Task<RetrievDataResponse> RetrievLatestData(RetrievDataRequest req);
        bool IsValidDevice(int id);
        bool IsValidSensor(int id);
    }
}
