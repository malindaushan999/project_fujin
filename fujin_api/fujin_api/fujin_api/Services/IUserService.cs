﻿using Fujin.DataContracts.Base;
using Fujin.DataContracts.User;
using Fujin.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Fujin.DataContracts.Enums;

namespace Fujin.API.Services
{
    public interface IUserService
    {
        m_user GetUserById(int id);
        bool GetUserStatus(string sessionId);

        Task<AuthResponse> Authenticate(AuthRequest req);
        Task<BaseResponse> Logout();

        Task<UserResponse> GetUserAsync(int id);
        Task<UserListResponse> GetAllUsersAsync();
        Task<BaseResponse> UpsertAsync(UPSERT_MODE upsertMode, UserUpsertRequest req);
        Task<BaseResponse> DeleteAsync(int userid);
        Task<BaseResponse> ResetPasswordAsync(int userid);
    }
}
