﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fujin.Common
{
    public static class StaticData
    {
        public static readonly int SYS_USERID = 1;
        public static readonly string SYS_USERNAME = "sysadmin";
    }
}
