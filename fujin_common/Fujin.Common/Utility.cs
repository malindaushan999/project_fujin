﻿using Fujin.DB;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Fujin.Common
{
    public static class APIUtility
    {
        public static string CreateConnectionString(DatabaseConfig dbConfig)
        {
            string connString = string.Format("Host={0};Port={1};Database={2};Username={3};Password={4};Include Error Detail=true",
                dbConfig?.Host ?? "localhost",
                dbConfig?.Port ?? "5432",
                Encoding.UTF8.GetString(Convert.FromBase64String(dbConfig?.Database)),
                Encoding.UTF8.GetString(Convert.FromBase64String(dbConfig?.Username)),
                Encoding.UTF8.GetString(Convert.FromBase64String(dbConfig?.Password)));

            return connString;
        }

        public static string CreateConnectionString(MongoDbConfig dbConfig)
        {
            string connString = string.Empty;

            if (string.IsNullOrEmpty(dbConfig.Username) || string.IsNullOrEmpty(dbConfig.Password))
            {
                connString = string.Format("mongodb://{0}:{1}/{2}",
                    dbConfig?.Host ?? "localhost",
                    dbConfig?.Port ?? "27017",
                    Encoding.UTF8.GetString(Convert.FromBase64String(dbConfig?.Database)));
            }
            else
            {
                connString = string.Format("mongodb://{3}:{4}@{0}:{1}/{2}",
                    dbConfig?.Host ?? "localhost",
                    dbConfig?.Port ?? "27017",
                    Encoding.UTF8.GetString(Convert.FromBase64String(dbConfig?.Database)),
                    Encoding.UTF8.GetString(Convert.FromBase64String(dbConfig?.Username)),
                    Encoding.UTF8.GetString(Convert.FromBase64String(dbConfig?.Password)));
            }

            return connString;
        }

        public static string GenerateJwtToken(string secret, m_user user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.user_id.ToString()) }),
                Expires = DateTime.UtcNow.AddHours(3),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public static string GenerateJwtToken(string secret, m_device device)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("device_id", device.device_id.ToString()) }),
                Expires = DateTime.UtcNow.AddMonths(3),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        /// <summary>
        /// Encode Base64 
        /// </summary>
        /// <param name="plainText">Input text</param>
        /// <returns>Encoded string</returns>
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        /// <summary>
        /// Decode Base64
        /// </summary>
        /// <param name="base64EncodedData">Input encoded text</param>
        /// <returns>Decoded string</returns>
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
