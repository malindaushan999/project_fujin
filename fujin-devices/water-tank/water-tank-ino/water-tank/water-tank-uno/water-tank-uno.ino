#include "NewPing.h"
#include "DHT.h"

const int trigPin = 10;  //D4
const int echoPin = 11;  //D3

#define DHTPIN 6     // Digital pin connected to the DHT sensor
#define DHTTYPE DHT11   // DHT 11

DHT dht(DHTPIN, DHTTYPE);
NewPing sonar(trigPin, echoPin);

float distance = 0.0;
float temparature = 0.0;
float humidity = 0.0;

String CreateRequestDataJson();
void SendDataToESP();
void RetrievDataFromESP();
void ReadSensorData();

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  dht.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  ReadSensorData();
  delay(500);

  SendDataToESP();
  delay(500);
}

void SendDataToESP() {
  String dataToSend = String(distance) + "|";
  dataToSend += String(temparature) + "|";
  dataToSend += String(humidity) + ",";
  Serial.println(dataToSend);
}

void RetrievDataFromESP() {
  bool StringReady;
  String httpResponse = "";

  if (!Serial.available())
    return;
  while (Serial.available()) {
    httpResponse = Serial.readString();
    StringReady = true;
  }
  if (StringReady) {
    Serial.println(httpResponse);
  }
}

void ReadSensorData()
{
  distance = sonar.ping_cm(200);
  temparature = dht.readTemperature(); 
  humidity = dht.readHumidity();
}
