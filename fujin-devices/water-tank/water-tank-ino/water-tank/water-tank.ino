#include "ESP8266WiFi.h"
#include "ESP8266HTTPClient.h"

// WiFi parameters to be configured
const char *ssid = "USHI-D";
const char *password = "Dialog4G@USHI";

//#define FUJINAPI_SERVER_IP "4.238.67.15"
#define FUJINAPI_SERVER_IP "54.238.67.15"
#define UPLOAD_DATA_API "/FujinAPI/Device/UploadData"
#define API_KEY "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkZXZpY2VfaWQiOiIxIiwibmJmIjoxNjIyOTg0OTcyLCJleHAiOjE2MzA5MzM3NzIsImlhdCI6MTYyMjk4NDk3Mn0.IKt7THDhTFQvdAmQa9PU4Xd77DLzD0O3FzkjPOmcIwY"

const int trigPin = 10;  //D4
const int echoPin = 11;  //D3

String distance;
String temparature;
String humidity;

void RetrievDataFromArduino();
String CreateRequestDataJson();
String SplitString(String data, char separator, int index);
String SplitStringLast(String data, char seperator);

void setup()
{
  // Begin serial moniter
  Serial.begin(115200);

  // Connect to WiFi
  WiFi.begin(ssid, password);
}

void loop()
{
  // Check WiFi connection status
  if(WiFi.status() != WL_CONNECTED)
  {
    Serial.println("");
    Serial.print("Connecting to WiFi");

    // while wifi not connected yet, print '.'
    // then after it connected, get out of the loop
    while (WiFi.status() != WL_CONNECTED)
    {
      delay(500);
      Serial.print(".");
    }

    //print a new line, then print WiFi connected and the IP address
    Serial.println("");
    Serial.println("WiFi connected");
    // Print the IP address
    Serial.println(WiFi.localIP());
  }

  WiFiClient client;
  HTTPClient httpClient;

  Serial.print("[HTTP] begin...\n");
  httpClient.begin(client, "http://" FUJINAPI_SERVER_IP UPLOAD_DATA_API);
  httpClient.addHeader("Content-Type", "application/json");
  httpClient.addHeader("x-device-auth", API_KEY); 
  delay(500);

  RetrievDataFromArduino();
  delay(500);

  Serial.print("[HTTP] POST...\n");
  String postData = CreateRequestDataJson();
  Serial.println(postData);
  delay(500);

  int httpCode = httpClient.POST(postData);

  // httpCode will be negative on error
  if (httpCode > 0) 
  {
    // HTTP header has been send and Server response header has been handled
    Serial.printf("[HTTP] POST... code: %d\n", httpCode);

    // file found at server
    if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_PARTIAL_CONTENT) {
      const String& payload = httpClient.getString();
      Serial.println("Response:\n\t<<");
      Serial.println("\t" + payload);
      Serial.println("\t>>");
    }
    else {
      const String& payload = httpClient.getString();
      Serial.println("Response:\n[ERROR]\n\t<<");
      Serial.println("\t" + payload);
      Serial.println("\t>>");
    }
  } 
  else 
  {
    Serial.printf("[HTTP] POST... failed, code: %d, error: %s\n", httpCode, httpClient.errorToString(httpCode).c_str());
  }

  httpClient.end();

  delay(30 * 1000); // 30sec
}

const byte numChars = 32;
char receivedChars[numChars];

void RetrievDataFromArduino()
{
  bool StringReady;
  String serialReading;

  while (Serial.available())
  {
    serialReading = Serial.readString();
    StringReady = true;
  }
  if (StringReady) 
  {
      //Serial.println("[Retrievd from Arduino serialReading] => " + serialReading);
      String stringLine = SplitStringLast(serialReading, ',');
      distance = SplitString(stringLine, '|', 0);
      temparature = SplitString(stringLine, '|', 1);
      humidity = SplitString(stringLine, '|', 2);
      Serial.println("[Retrievd from Arduino] => " + stringLine);
  }
}

String CreateRequestDataJson()
{
  String json = "";
  json  = "[";
  json += "{";
  json += "\"SensorID\":" + String(1) + ",";
  json += "\"DataValue\":\"" + distance + "\"";
  json += "},";
  json += "{";
  json += "\"SensorID\":" + String(2) + ",";
  json += "\"DataValue\":\"" + temparature + "\"";
  json += "},";
  json += "{";
  json += "\"SensorID\":" + String(3) + ",";
  json += "\"DataValue\":\"" + humidity + "\"";
  json += "}";
  json += "]";

  return json;
}

String SplitString(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

String SplitStringLast(String data, char seperator)
{
  String result;
  int lastIndex = data.lastIndexOf(seperator);
  result = data.substring(lastIndex + 1, data.length());
  
  if(data.indexOf(seperator) == lastIndex && (result == "" || result == "\n" || result == "\r\n"))
  {
  	return data.substring(0, lastIndex);
  }
  
  if(result == "" || result == "\n" || result == "\r\n")
  {
    int startIndexPadding = 0;
    if(result == "") startIndexPadding++;
    if(result == "\n") startIndexPadding += 2;
    if(result == "\r\n") startIndexPadding += 3;
    
    for(int i = lastIndex - 1; i > 0; i--)
    {
    	if(data.charAt(i) == seperator)
        {
        	result = data.substring(i + startIndexPadding, lastIndex);
          	break;
        }
    }
  }
  
  return result;
}