﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fujin.DataContracts.Base
{
    public enum MessageType
    {
        Success,
        Info,
        Warning,
        Error
    }

    public class BaseResponse
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public int Type { get; set; }

        public BaseResponse(bool createTestMessage = false)
        {
            if (createTestMessage)
            {
                Code = ErrorCode.SYS_E10002;
                Type = 3; // Error
                Message = "Test Message";
            }
        }
    }
}
