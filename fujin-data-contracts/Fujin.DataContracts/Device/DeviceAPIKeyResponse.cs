﻿using Fujin.DataContracts.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fujin.DataContracts.Device
{
    public class DeviceAPIKeyResponse : BaseResponse
    {
        public string APIKey { get; set; }
    }
}
