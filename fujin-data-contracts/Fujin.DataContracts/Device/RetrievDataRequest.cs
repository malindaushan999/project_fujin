﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fujin.DataContracts.Device
{
    public class RetrievDataRequest
    {
        [Required]
        public int TargetDeviceID { get; set; }
        [Required]
        public int TargetSensorID { get; set; }
    }
}
