﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fujin.DataContracts.Device
{
    public class DataUploadEntity
    {
        public int SensorID { get; set; }
        public string DataValue { get; set; }
    }
}
