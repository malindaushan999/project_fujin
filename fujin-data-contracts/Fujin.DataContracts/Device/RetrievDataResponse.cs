﻿using Fujin.DataContracts.Base;
using Fujin.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fujin.DataContracts.Device
{
    public class RetrievDataResponse : BaseResponse
    {
        public List<ResDataSensor> DataList { get; set; } = new(); 
    }

    public class ResDataSensor
    {
        public int SensorID { get; set; }
        public double DataValue { get; set; }
        public DateTime Timestamp { get; set; }
        public List<KeyValuePair<int, double>> ThresholdList { get; set; } = new();
    }
}
