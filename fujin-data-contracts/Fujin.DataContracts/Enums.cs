﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fujin.DataContracts
{
    public static class Enums
    {
        public enum DELETE_FLAG
        {
            NOT_DELETE = 0,
            DELETE = 1
        }

        public enum ACTIVE_FLAG
        {
            ACTIVE = 0,
            INACTIVE = 1
        }

        public enum ERROR_TYPE
        {
            SUCCESS = 0,
            INFO = 1,
            WARNING = 2,
            ERROR = 3
        }

        public enum UPSERT_MODE
        {
            INSERT = 0,
            UPDATE = 1
        }
    }
}
