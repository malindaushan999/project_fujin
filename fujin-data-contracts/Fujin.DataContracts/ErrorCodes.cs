﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fujin.DataContracts
{
    public static class ErrorCode
    {
        #region System Errors (10000-10999)

        /// <summary>
        /// Exception occurred.
        /// </summary>
        public static readonly string SYS_E10000 = "10000"; 
        /// <summary>
        /// Unauthorized login attempt.
        /// </summary>
        public static readonly string SYS_E10001 = "10001";
        /// <summary>
        /// System default message.
        /// </summary>
        public static readonly string SYS_E10002 = "10002";
        /// <summary>
        /// JSON serialization failed.
        /// </summary>
        public static readonly string SYS_E10003 = "10003";

        #endregion System Errors (10000-10999)

        #region User Errors (11000-11999)

        /// <summary>
        /// Login is successful
        /// </summary>
        public static readonly string USER_S11000 = "11000";
        /// <summary>
        /// User retrieval is successful.
        /// </summary>
        public static readonly string USER_S11001 = "11001";
        /// <summary>
        /// Username or password is incorrect.
        /// </summary>
        public static readonly string USER_E11002 = "11002";
        /// <summary>
        /// Logout failed.
        /// </summary>
        public static readonly string USER_E11003 = "11003";
        /// <summary>
        /// Logout successful.
        /// </summary>
        public static readonly string USER_S11004 = "11004";
        /// <summary>
        /// User list retrieval is successful.
        /// </summary>
        public static readonly string USER_S11005 = "11005";
        /// <summary>
        /// User list retrieval failed.
        /// </summary>
        public static readonly string USER_E11006 = "11006";
        /// <summary>
        /// User created successfully.
        /// </summary>
        public static readonly string USER_S11007 = "11007";
        /// <summary>
        /// Failed to create the user.
        /// </summary>
        public static readonly string USER_E11008 = "11008";
        /// <summary>
        /// User updated successfully.
        /// </summary>
        public static readonly string USER_S11009 = "11009";
        /// <summary>
        /// Failed to update the user.
        /// </summary>
        public static readonly string USER_E11010 = "11010";
        /// <summary>
        /// Username already exists.
        /// </summary>
        public static readonly string USER_E11011 = "11011";
        /// <summary>
        /// User doesn't exists.
        /// </summary>
        public static readonly string USER_E11012 = "11012";
        /// <summary>
        /// User data validation failed.
        /// </summary>
        public static readonly string USER_E11013 = "11013";
        /// <summary>
        /// User deleted successfully.
        /// </summary>
        public static readonly string USER_S11014 = "11014";
        /// <summary>
        /// Failed to delete the user.
        /// </summary>
        public static readonly string USER_E11015 = "11015";
        /// <summary>
        /// Cannot delete yourself.
        /// </summary>
        public static readonly string USER_E11016 = "11016";
        /// <summary>
        /// Cannot edit this user.
        /// </summary>
        public static readonly string USER_E11017 = "11017";
        /// <summary>
        /// Cannot delete this user.
        /// </summary>
        public static readonly string USER_E11018 = "11018";
        /// <summary>
        /// User retrieval failed.
        /// </summary>
        public static readonly string USER_E11019 = "11019";
        /// <summary>
        /// Password reset successful.
        /// </summary>
        public static readonly string USER_S11020 = "11020";

        #endregion User Errors (11000-11999)

        #region Device Errors (12000-12999)

        /// <summary>
        /// APIKey created successfully.
        /// </summary>
        public static readonly string DEVICE_S12000 = "12000";
        /// <summary>
        /// APIKey creation task failed.
        /// </summary>
        public static readonly string DEVICE_E12001 = "12001";
        /// <summary>
        /// Data uploaded successfully.
        /// </summary>
        public static readonly string DEVICE_S12002 = "12002";
        /// <summary>
        /// Data upload task failed.
        /// </summary>
        public static readonly string DEVICE_E12003 = "12003";
        /// <summary>
        /// Unrecognized device.
        /// </summary>
        public static readonly string DEVICE_E12004 = "12004";
        /// <summary>
        /// Data retrieved successfully.
        /// </summary>
        public static readonly string DEVICE_S12005 = "12005";
        /// <summary>
        /// Data retriev task failed.
        /// </summary>
        public static readonly string DEVICE_E12006 = "12006";
        /// <summary>
        /// Invalid device ID request param.
        /// </summary>
        public static readonly string DEVICE_E12007 = "12007";
        /// <summary>
        /// Invalid sensor ID request param.
        /// </summary>
        public static readonly string DEVICE_E12008 = "12008";

        #endregion Device Errors (12000-12999)
    }
}
