﻿using Fujin.DataContracts.Base;
using Fujin.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fujin.DataContracts.User
{
    public class UserResponse : BaseResponse
    {
        public m_user User { get; set; }
    }
}
