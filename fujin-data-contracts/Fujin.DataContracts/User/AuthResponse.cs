﻿using Fujin.DataContracts.Base;
using Fujin.DB;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fujin.DataContracts.User
{
    public class AuthResponse : BaseResponse
    {
        public string AuthToken { get; set; }
        public m_user UserInfo { get; set; } = new m_user();
    }
}
