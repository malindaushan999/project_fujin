﻿using Fujin.DataContracts.Base;
using Fujin.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fujin.DataContracts.User
{
    public class UserListResponse : BaseResponse
    {
        public List<m_user> UserList { get; set; } = new();
    }
}
