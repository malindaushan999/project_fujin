﻿$(function () {
    $("#datatable-buttons")
        .DataTable({
            //lengthChange: !1,
            dom: 'Bfrtip',
            buttons: ["csv", "excel", "pdf", "print"],
            lengthMenu: [10],
            ordering: true
        })
        .buttons()
        .container()
        .appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)");
});
