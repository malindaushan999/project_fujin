﻿$(function () {
    var t = document.getElementsByClassName("needs-validation");
    Array.prototype.filter.call(t, function (e) {
        e.addEventListener(
            "submit",
            function (t) {
                !1 === e.checkValidity() && (t.preventDefault(), t.stopPropagation()), e.classList.add("was-validated");
            },
            !1
        );
    });
});

$(function () {
    $(".custom-validation").parsley();
});

$(function () {
    $.validator.unobtrusive.parse(".needs-validation");
})