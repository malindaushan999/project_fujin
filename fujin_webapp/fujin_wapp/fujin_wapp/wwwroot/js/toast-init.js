﻿function ShowToastMessage(type, title, message) {
    // Add options
    toastr.options = {
        closeButton: true
    };

    if (title !== '') {
        switch (type) {
            case 'error':
                toastr.error(message, title);
                break;
            case 'success':
                toastr.success(message, title);
                break;
            case 'info':
                toastr.info(message, title);
                break;
            case 'warning':
                toastr.warning(message, title);
                break;
            default:
        }
    }
    else {
        switch (type) {
            case 'error':
                toastr.error(message);
                break;
            case 'success':
                toastr.success(message);
                break;
            case 'info':
                toastr.info(message);
                break;
            case 'warning':
                toastr.warning(message);
                break;
            default:
        }
    }
}

function ShowToastQuestion(type, question, defaultBtnText, defaultBtnLink) {

    // Clear toast mesasages
    toastr.clear();

    var btnCss = "btn-default";
    switch (type) {
        case 'error':
            btnCss = "btn-danger";
            break;
        case 'info':
        case 'success':
        case 'warning':
            btnCss = "btn-primary";
            break;
        default:
    }

    // Create toast body
    var tostrBody = "";
    tostrBody += "<div>";
    tostrBody += question;
    tostrBody += "<br>";
    tostrBody += "<br>";
    tostrBody += "<a class='btn " + btnCss + " float-right waves-effect waves-light' href='" + defaultBtnLink + "'>" + defaultBtnText + "</a>";
    tostrBody += "</div>";

    // Add options
    toastr.options = {
        closeButton: true,
        positionClass: "toast-top-center",
        timeOut: 0,
        extendedTimeOut: 0,
        tapToDismiss: false
    };

    switch (type) {
        case 'error':
            toastr.error(tostrBody);
            break;
        case 'success':
            toastr.success(tostrBody);
            break;
        case 'info':
            toastr.info(tostrBody);
            break;
        case 'warning':
            toastr.warning(tostrBody);
            break;
        default:
    }
}