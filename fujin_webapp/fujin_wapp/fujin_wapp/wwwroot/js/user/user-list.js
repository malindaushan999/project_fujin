﻿function LoadModalPopup(url) {
    toastr.clear();
    $('#divModalArea').html('');

    $.ajax({
        method: "GET",
        url: url,
        success: function (resDiv) {
            $('#divModalArea').html(resDiv);
            $('#modal-popup').modal('show');
        },
        error: function myfunction(res) {
            console.error(res);
            location.href = '/Home/Error';
        }
    });
}