﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace Fujin.WApp.Common
{
    public static class SessionExtensions
    {
        /// <summary>
        /// Convert object to json and store it in the session
        /// </summary>
        /// <param name="session">Session</param>
        /// <param name="key">Key</param>
        /// <param name="value">object</param>
        public static void SetObjectAsJson(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        /// <summary>
        /// Convert json from the session to generic object
        /// </summary>
        /// <typeparam name="T">Generic type</typeparam>
        /// <param name="session">Session</param>
        /// <param name="key">Key</param>
        /// <returns>Converted object</returns>
        public static T GetObjectFromJson<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
}
