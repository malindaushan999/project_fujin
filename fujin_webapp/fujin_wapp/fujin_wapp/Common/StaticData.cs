﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fujin.WApp.Common
{
    public static class StaticData
    {
        public static class ApiUrls
        {
            // Base API
            public static string BaseUrl = "https://localhost:444/FujinAPI";

            // /User
            public static readonly string Authenticate = "User/Authenticate";
            public static readonly string Logout = "User/Logout";
            public static readonly string UserList = "User/UserList";
            public static readonly string GetUser = "User/GetUser";
            public static readonly string CreateNewUser = "User/CreateNewUser";
            public static readonly string EditUser = "User/EditUser";
            public static readonly string DeleteUser = "User/DeleteUser";
            public static readonly string ResetPassword = "User/ResetPassword";

            /// <summary>
            /// Create Url (BaseUrl + Path)
            /// </summary>
            /// <param name="path">Path</param>
            /// <returns>Url</returns>
            public static string CreateUrl(string path)
            {
                return String.Format("{0}/{1}", BaseUrl, path);
            }

            /// <summary>
            /// Create Url (BaseUrl + Path + QueryValue)
            /// </summary>
            /// <param name="path">Path</param>
            /// <param name="queryValue">QueryValue</param>
            /// <returns>Url</returns>
            public static string CreateUrl(string path, string queryValue)
            {
                return String.Format("{0}/{1}/{2}", BaseUrl, path, queryValue);
            }

            /// <summary>
            /// Create Url (BaseUrl + Path + QueryParamList)
            /// </summary>
            /// <param name="path">Path</param>
            /// <param name="queryParamList">QueryParamList</param>
            /// <returns>Url</returns>
            public static string CreateUrl(string path, List<KeyValuePair<string,string>> queryParamList)
            {
                string url = String.Format("{0}/{1}?", BaseUrl, path);
                foreach (var param in queryParamList)
                {
                    url += string.Format("{0}={1}&", param.Key, param.Value); 
                }
                url = url.Remove(url.Length - 1);

                return url;
            }


        }

        public static class ToastType
        {
            public static readonly string Error = "error";
            public static readonly string Success = "success";
            public static readonly string Info = "info";
            public static readonly string Warning = "warning";
        }
    }
}
