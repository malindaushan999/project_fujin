﻿using Fujin.WApp.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fujin.WApp.Common
{
    public class SessionStore
    {
        public string AuthKey { get; set; }
        public UserViewModel User { get; set; } = new();
    }
}
