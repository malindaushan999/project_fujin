﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fujin.WApp.Common
{
    public static class ErrorMessages
    {
        /// <summary>
        /// Common Error Messages
        /// </summary>
        public static class Common
        {
            public const string ExceptionOccurred = "Exception occurred.";
            public const string ModelValidationFailed = "Server side model validation failed.";
        }

        /// <summary>
        /// Model Data Annotation Error Messages
        /// </summary>
        public static class Model
        {
            public static class Common
            {
                public const string MaxLength20 = "Maximum 20 chars are allowed.";
                public const string MaxLength32 = "Maximum 32 chars are allowed.";
                public const string MaxLength64 = "Maximum 64 chars are allowed.";
                public const string MaxLength128 = "Maximum 128 chars are allowed.";
                public const string MaxLength256 = "Maximum 256 chars are allowed.";
            }

            public static class LoginViewModel
            {
                public const string UsernameRequired = "Please enter the username.";
                public const string PasswordRequired = "Please enter the password.";
            }

            public static class UserViewModel
            {
                public const string UsernameRequired = "Please enter the username.";
                public const string PasswordRequired = "Please enter the password.";
                public const string FirstNameRequired = "Please enter your first name.";
                public const string EmailRequired = "Please enter your email address.";
                public const string EmailValidity = "Please enter a valid email address.";
                public const string PhoneValidity = "Please enter a valid phone number.";
                public const string PasswordMinLength = "Please enter more than 6 characters.";
            }

            public static class CreateUserModel
            {
                public const string ConfirmPasswordRequired = "Please re-enter the password.";
                public const string ConfirmPasswordCompare = "Passwords do not match.";
            }

            public static class EditUserModel
            {
                public const string UserIDRequired = "Please enter the user id.";
            }
        }
    }
}
