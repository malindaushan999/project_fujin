﻿using Fujin.DB;
using Fujin.WApp.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Fujin.WApp.Models.User
{
    public class EditUserModel : UserViewModel
    {
        [Required(ErrorMessage = ErrorMessages.Model.EditUserModel.UserIDRequired)]
        public new int UserID { get; set; }

        public static EditUserModel ToEditUserModel(m_user user)
        {
            return new EditUserModel()
            {
                UserID = user.user_id,
                Username = user.username,
                FirstName = user.f_name,
                LastName = user.l_name,
                Address = user.address,
                Phone = user.phone,
                Email = user.email
            };
        }
    }
}
