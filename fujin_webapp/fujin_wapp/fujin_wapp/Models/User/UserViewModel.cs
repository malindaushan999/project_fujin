﻿using Fujin.DB;
using Fujin.WApp.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Fujin.WApp.Models.User
{
    public class UserViewModel
    {
        [Display(Name = "User ID")]
        public int UserID { get; set; }

        [Display(Name = "Username")]
        [Required(ErrorMessage = ErrorMessages.Model.UserViewModel.UsernameRequired)]
        [MaxLength(32, ErrorMessage = ErrorMessages.Model.Common.MaxLength32)]
        public string Username { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = ErrorMessages.Model.UserViewModel.FirstNameRequired)]
        [MaxLength(64, ErrorMessage = ErrorMessages.Model.Common.MaxLength64)]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [MaxLength(64, ErrorMessage = ErrorMessages.Model.Common.MaxLength64)]
        public string LastName { get; set; }

        [MaxLength(256, ErrorMessage = ErrorMessages.Model.Common.MaxLength256)]
        public string Address { get; set; }
        
        [MaxLength(20, ErrorMessage = ErrorMessages.Model.Common.MaxLength20)]
        [Phone(ErrorMessage = ErrorMessages.Model.UserViewModel.PhoneValidity)]
        public string Phone { get; set; }

        [Required(ErrorMessage = ErrorMessages.Model.UserViewModel.EmailRequired)]
        [EmailAddress(ErrorMessage = ErrorMessages.Model.UserViewModel.EmailValidity)]
        public string Email { get; set; }

        public static UserViewModel ToUserViewModel(m_user user)
        {
            return new UserViewModel()
            {
                UserID = user.user_id,
                Username = user.username,
                FirstName = user.f_name,
                LastName = user.l_name,
                Address = user.address,
                Phone = user.phone,
                Email = user.email
            };
        }
    }
}
