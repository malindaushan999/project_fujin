using Fujin.WApp.Common;
using System;

namespace Fujin.WApp.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);

        public string Title { get; set; } = ErrorMessages.Common.ExceptionOccurred.Substring(0, ErrorMessages.Common.ExceptionOccurred.Length - 1);

        public string Message { get; set; }
    }
}
