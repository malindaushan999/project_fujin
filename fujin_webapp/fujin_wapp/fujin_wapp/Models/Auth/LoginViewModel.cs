﻿using Fujin.WApp.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Fujin.WApp.Models.Auth
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = ErrorMessages.Model.LoginViewModel.UsernameRequired)]
        public string Username { get; set; }

        [Required(ErrorMessage = ErrorMessages.Model.LoginViewModel.PasswordRequired)]
        public string Password { get; set; }
    }
}
