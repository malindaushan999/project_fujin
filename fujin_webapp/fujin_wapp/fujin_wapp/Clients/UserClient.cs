﻿using Fujin.DataContracts.Base;
using Fujin.DataContracts.User;
using Fujin.DB;
using Fujin.WApp.Common;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Fujin.WApp.Clients
{
    public class UserClient : BaseClient, IUserClient
    {
        /// <summary>
        /// Constrctor
        /// </summary>
        /// <param name="httpClient">Http client</param>
        public UserClient(HttpClient httpClient, IHttpContextAccessor httpContextAccessor) : base(httpClient, httpContextAccessor, true)
        {

        }

        #region Public

        /// <summary>
        /// GetUserAsync
        /// </summary>
        /// <returns></returns>
        public async Task<UserResponse> GetUserAsync(int userID)
        {
            // Create query param list
            List<KeyValuePair<string, string>> paramList = new();
            paramList.Add(new KeyValuePair<string, string>("userID", userID.ToString()));

            // Create Url
            string url = StaticData.ApiUrls.CreateUrl(StaticData.ApiUrls.GetUser, paramList);

            // Get response
            HttpResponseMessage response = await _httpClient.GetAsync(url);

            response.EnsureSuccessStatusCode();

            // Extract response content
            var resData = await response.Content.ReadAsStringAsync();
            var res = JsonConvert.DeserializeObject<UserResponse>(resData);

            return res;
        }

        /// <summary>
        /// GetUserListAsync
        /// </summary>
        /// <returns>UserList</returns>
        public async Task<UserListResponse> GetUserListAsync()
        {
            // Create Url
            string url = StaticData.ApiUrls.CreateUrl(StaticData.ApiUrls.UserList);

            // Get response
            HttpResponseMessage response = await _httpClient.GetAsync(url);

            response.EnsureSuccessStatusCode();

            // Extract response content
            var resData = await response.Content.ReadAsStringAsync();
            var res = JsonConvert.DeserializeObject<UserListResponse>(resData);

            return res;
        }

        /// <summary>
        /// CreateUserAsync
        /// </summary>
        /// <param name="req">Request param</param>
        /// <returns>Response</returns>
        public async Task<BaseResponse> CreateUserAsync(UserUpsertRequest req)
        {
            // Create Url
            string url = StaticData.ApiUrls.CreateUrl(StaticData.ApiUrls.CreateNewUser);

            // Create request content
            string json = JsonConvert.SerializeObject(req);
            StringContent content = new(json, Encoding.UTF8, "application/json");

            // Get response
            HttpResponseMessage response = await _httpClient.PostAsync(url, content);

            response.EnsureSuccessStatusCode();

            // Extract response content
            var resData = await response.Content.ReadAsStringAsync();
            var res = JsonConvert.DeserializeObject<BaseResponse>(resData);

            return res;
        }

        /// <summary>
        /// EditUserAsync
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public async Task<BaseResponse> EditUserAsync(UserUpsertRequest req)
        {
            // Create Url
            string url = StaticData.ApiUrls.CreateUrl(StaticData.ApiUrls.EditUser);

            // Create request content
            string json = JsonConvert.SerializeObject(req);
            StringContent content = new(json, Encoding.UTF8, "application/json");

            // Get response
            HttpResponseMessage response = await _httpClient.PostAsync(url, content);

            response.EnsureSuccessStatusCode();

            // Extract response content
            var resData = await response.Content.ReadAsStringAsync();
            var res = JsonConvert.DeserializeObject<BaseResponse>(resData);

            return res;
        }

        /// <summary>
        /// DeleteUserAsync
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public async Task<BaseResponse> DeleteUserAsync(int userID)
        {
            // Create query param list
            List<KeyValuePair<string, string>> paramList = new();
            paramList.Add(new KeyValuePair<string, string>("userID", userID.ToString()));

            // Create Url
            string url = StaticData.ApiUrls.CreateUrl(StaticData.ApiUrls.DeleteUser, paramList);

            // Get response
            HttpResponseMessage response = await _httpClient.DeleteAsync(url);

            response.EnsureSuccessStatusCode();

            // Extract response content
            var resData = await response.Content.ReadAsStringAsync();
            var res = JsonConvert.DeserializeObject<BaseResponse>(resData);

            return res;
        }

        /// <summary>
        /// ResetPasswordAsync
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public async Task<BaseResponse> ResetPasswordAsync(int userID)
        {
            // Create query param list
            List<KeyValuePair<string, string>> paramList = new();
            paramList.Add(new KeyValuePair<string, string>("userID", userID.ToString()));

            // Create Url
            string url = StaticData.ApiUrls.CreateUrl(StaticData.ApiUrls.ResetPassword, paramList);

            // Get response
            HttpResponseMessage response = await _httpClient.GetAsync(url);

            response.EnsureSuccessStatusCode();

            // Extract response content
            var resData = await response.Content.ReadAsStringAsync();
            var res = JsonConvert.DeserializeObject<BaseResponse>(resData);

            return res;
        }

        #endregion Public
    }
}
