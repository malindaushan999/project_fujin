﻿using Fujin.DataContracts.Base;
using Fujin.DataContracts.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Fujin.WApp.Clients
{
    public interface IAuthClient
    {
        public Task<AuthResponse> Login(AuthRequest req);
        public Task<BaseResponse> Logout();
    }
}
