﻿using Fujin.WApp.Common;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Fujin.WApp.Clients
{
    public class BaseClient
    {
        protected readonly HttpClient _httpClient;
        protected readonly ILogger<AuthClient> _logger;
        protected readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="httpClient">Http Client</param>
        /// <param name="httpContextAccessor">Http Context Accessor</param>
        /// <param name="addHeaders"></param>
        public BaseClient(HttpClient httpClient, IHttpContextAccessor httpContextAccessor, bool addHeaders = true)
        {
            HttpClientHandler httpClientHandler = new()
            {
                ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; }
            };

            _httpClient = new HttpClient(httpClientHandler);
            _httpContextAccessor = httpContextAccessor;

            if (addHeaders)
            {
                AddHeaders();
            }
        }

        /// <summary>
        /// Add Headers to the request
        /// </summary>
        protected void AddHeaders()
        {
            // Remove all headers
            RemoveHeaders();

            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            // Add auth token to the header
            string authToken = ExtractAuthTokenFromSession();
            if (!string.IsNullOrEmpty(authToken))
            {
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", authToken);
            }

        }

        /// <summary>
        /// Remove All Headers
        /// </summary>
        protected void RemoveHeaders()
        {
            _httpClient.DefaultRequestHeaders.Clear();
        }

        /// <summary>
        /// Extract Auth token from session
        /// </summary>
        /// <returns>Auth token</returns>
        protected string ExtractAuthTokenFromSession()
        {
            SessionStore sessionData = _httpContextAccessor.HttpContext?.Session.GetObjectFromJson<SessionStore>("SessionDataStore");
            if (sessionData is not null && !string.IsNullOrEmpty(sessionData.AuthKey))
            {
                return sessionData.AuthKey;
            }

            return string.Empty;
        }

        /// <summary>
        /// Clear session
        /// </summary>
        protected void ClearSession()
        {
            if (_httpContextAccessor.HttpContext is not null)
            {
                _httpContextAccessor.HttpContext.Session.Clear();
            }
        }
    }
}
