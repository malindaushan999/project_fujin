﻿using Fujin.DataContracts.Base;
using Fujin.DataContracts.User;
using Fujin.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fujin.WApp.Clients
{
    public interface IUserClient
    {
        public Task<UserListResponse> GetUserListAsync();
        public Task<BaseResponse> CreateUserAsync(UserUpsertRequest req);
        public Task<BaseResponse> EditUserAsync(UserUpsertRequest req);
        public Task<UserResponse> GetUserAsync(int userID);
        public Task<BaseResponse> DeleteUserAsync(int userID);
        public Task<BaseResponse> ResetPasswordAsync(int userID);
    }
}
