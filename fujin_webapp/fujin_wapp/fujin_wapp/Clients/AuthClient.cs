﻿using Fujin.DataContracts.Base;
using Fujin.DataContracts.User;
using Fujin.WApp.Common;
using Fujin.WApp.Models.User;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Fujin.WApp.Clients
{
    public sealed class AuthClient : BaseClient, IAuthClient
    {
        /// <summary>
        /// Constrctor
        /// </summary>
        /// <param name="httpClient">Http client</param>
        public AuthClient(HttpClient httpClient, IHttpContextAccessor httpContextAccessor) : base(httpClient, httpContextAccessor, true)
        {

        }

        #region Private

        /// <summary>
        /// Create session
        /// </summary>
        /// <param name="resData">session data</param>
        private void CreateSession(AuthResponse resData)
        {
            // Clear session data
            ClearSession();

            // Create session
            SessionStore sessionStore = new()
            {
                AuthKey = resData.AuthToken,
                User = UserViewModel.ToUserViewModel(resData.UserInfo)
            };

            // Set session
            if (_httpContextAccessor.HttpContext is not null)
            {
                _httpContextAccessor.HttpContext.Session.SetObjectAsJson("SessionDataStore", sessionStore);
            }
        }

        #endregion Private

        #region Public

        /// <summary>
        /// Login Request
        /// </summary>
        /// <param name="req">Request object</param>
        /// <returns>Login response</returns>
        public async Task<AuthResponse> Login(AuthRequest req)
        {
            // Create Url
            string url = StaticData.ApiUrls.CreateUrl(StaticData.ApiUrls.Authenticate);

            // Create request content
            string json = JsonConvert.SerializeObject(req);
            StringContent content = new(json, Encoding.UTF8, "application/json");

            // Get response
            HttpResponseMessage response = await _httpClient.PostAsync(url, content);

            // Check response status code
            switch (response.StatusCode)
            {
                case HttpStatusCode.Unauthorized:
                    break;
                default:
                    response.EnsureSuccessStatusCode();
                    break;
            }

            // Extract response content
            var resData = await response.Content.ReadAsStringAsync();
            var authResponse = JsonConvert.DeserializeObject<AuthResponse>(resData);

            // Add data to session
            CreateSession(authResponse);

            return authResponse;
        }

        /// <summary>
        /// Logout request
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>Logout status</returns>
        public async Task<BaseResponse> Logout()
        {
            // Create Url
            string url = StaticData.ApiUrls.CreateUrl(StaticData.ApiUrls.Logout);

            // Get response
            HttpResponseMessage response = await _httpClient.GetAsync(url);

            // Check response status code
            switch (response.StatusCode)
            {
                case HttpStatusCode.Unauthorized:
                    break;
                default:
                    response.EnsureSuccessStatusCode();
                    break;
            }

            // Remove Headers
            RemoveHeaders();

            // Clear Session
            ClearSession();

            // Extract response content
            var resData = await response.Content.ReadAsStringAsync();
            var logoutResponse = JsonConvert.DeserializeObject<BaseResponse>(resData);

            return logoutResponse;
        }

        #endregion Public
    }
}
