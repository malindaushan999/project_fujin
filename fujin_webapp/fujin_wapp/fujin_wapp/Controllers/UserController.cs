﻿using Fujin.DataContracts;
using Fujin.DataContracts.Base;
using Fujin.DataContracts.User;
using Fujin.WApp.Clients;
using Fujin.WApp.Common;
using Fujin.WApp.Models;
using Fujin.WApp.Models.User;
using Fujin.WApp.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Fujin.WApp.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserClient _userClient;
        private readonly IServiceProvider _serviceProvider;

        public UserController(ILogger<UserController> logger, IUserClient userClient, IServiceProvider serviceProvider)
        {
            _logger = logger;
            _userClient = userClient;
            _serviceProvider = serviceProvider;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            _logger.LogDebug("IN");
            try
            {
                UserListResponse res = await _userClient.GetUserListAsync();
                if (res.Code == ErrorCode.USER_S11005)
                {
                    List<UserViewModel> modelList = new();

                    foreach (var item in res.UserList)
                    {
                        UserViewModel model = UserViewModel.ToUserViewModel(item);
                        modelList.Add(model);
                    }

                    _logger.LogInformation("OK");
                    return View(modelList);
                }
                else
                {
                    _logger.LogError(res.Message);
                    TempData["ToastType"] = StaticData.ToastType.Error;
                    TempData["ToastMessage"] = res.Message;
                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ErrorMessages.Common.ExceptionOccurred);
                _logger.LogTrace("[EXCEPTION] " + ex.Message);
                if (ex.InnerException != null) _logger.LogTrace("[INNER EXCEPTION] " + ex.InnerException.Message);

                TempData["ErrorViewModel"] = JsonConvert.SerializeObject(new ErrorViewModel { Message = ex.Message });
                return RedirectToAction("Error", "Home");
            }
            finally
            {
                _logger.LogDebug("OUT");
            }
        }

        [HttpGet]
        public IActionResult CreateNewUserView()
        {
            _logger.LogDebug("IN");
            _logger.LogDebug("OUT");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateNewUserAction([FromForm] CreateUserModel model)
        {
            _logger.LogDebug("IN");
            try
            {
                if (ModelState.IsValid)
                {
                    UserUpsertRequest req = new()
                    {
                        Username = model.Username ?? string.Empty,
                        FirstName = model.FirstName ?? string.Empty,
                        LastName = model.LastName ?? string.Empty,
                        Email = model.Email ?? string.Empty,
                        Address = model.Address ?? string.Empty,
                        Phone = model.Phone ?? string.Empty
                    };

                    BaseResponse res = await _userClient.CreateUserAsync(req);
                    if (res.Code == ErrorCode.USER_S11007)
                    {
                        _logger.LogInformation("OK");
                        TempData["ToastType"] = StaticData.ToastType.Success;
                        TempData["ToastMessage"] = res.Message;
                        return RedirectToAction("Index", "User");
                    }
                    else
                    {
                        _logger.LogError(res.Message);
                        TempData["ToastType"] = StaticData.ToastType.Error;
                        TempData["ToastMessage"] = res.Message;
                        return RedirectToAction("Index", "User");
                    }
                }
                else
                {
                    _logger.LogError(ErrorMessages.Common.ModelValidationFailed);
                    TempData["ToastType"] = StaticData.ToastType.Warning;
                    TempData["ToastMessage"] = ErrorMessages.Common.ModelValidationFailed;
                    return RedirectToAction("Index", "User");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ErrorMessages.Common.ExceptionOccurred);
                _logger.LogTrace("[EXCEPTION] " + ex.Message);
                if (ex.InnerException != null) _logger.LogTrace("[INNER EXCEPTION] " + ex.InnerException.Message);

                TempData["ErrorViewModel"] = JsonConvert.SerializeObject(new ErrorViewModel { Message = ex.Message });
                return RedirectToAction("Error", "Home");
            }
            finally
            {
                _logger.LogDebug("OUT");
            }
        }

        [HttpGet]
        public async Task<IActionResult> EditUserView(int userID)
        {
            _logger.LogDebug("IN");
            try
            {
                UserResponse res = await _userClient.GetUserAsync(userID);
                if (res.Code == ErrorCode.USER_S11001)
                {
                    EditUserModel model = EditUserModel.ToEditUserModel(res.User);
                    model.UserID = userID;

                    _logger.LogInformation("OK");
                    return View(model);
                }
                else
                {
                    _logger.LogError(res.Message);
                    TempData["ToastType"] = StaticData.ToastType.Error;
                    TempData["ToastMessage"] = res.Message;
                    return RedirectToAction("Index", "User");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ErrorMessages.Common.ExceptionOccurred);
                _logger.LogTrace("[EXCEPTION] " + ex.Message);
                if (ex.InnerException != null) _logger.LogTrace("[INNER EXCEPTION] " + ex.InnerException.Message);

                TempData["ErrorViewModel"] = JsonConvert.SerializeObject(new ErrorViewModel { Message = ex.Message });
                return BadRequest(ErrorMessages.Common.ExceptionOccurred);
            }
            finally
            {
                _logger.LogDebug("OUT");
            }
        }

        [HttpPost]
        public async Task<IActionResult> EditUserAction([FromQuery] int userID, [FromForm] EditUserModel model)
        {
            _logger.LogDebug("IN");
            try
            {
                if (ModelState.IsValid)
                {
                    UserUpsertRequest req = new()
                    {
                        UserID = userID,
                        Username = model.Username ?? string.Empty,
                        FirstName = model.FirstName ?? string.Empty,
                        LastName = model.LastName ?? string.Empty,
                        Email = model.Email ?? string.Empty,
                        Address = model.Address ?? string.Empty,
                        Phone = model.Phone ?? string.Empty
                    };

                    BaseResponse res = await _userClient.EditUserAsync(req);
                    if (res.Code == ErrorCode.USER_S11009)
                    {
                        // Update session user
                        model.UserID = userID;
                        _serviceProvider.GetService<IHelperService>().UpdateSessionUser(model);

                        _logger.LogInformation("OK");
                        TempData["ToastType"] = StaticData.ToastType.Success;
                        TempData["ToastMessage"] = res.Message;
                        return RedirectToAction("Index", "User");
                    }
                    else
                    {
                        _logger.LogError(res.Message);
                        TempData["ToastType"] = StaticData.ToastType.Error;
                        TempData["ToastMessage"] = res.Message;
                        return RedirectToAction("Index", "User");
                    }
                }
                else
                {
                    _logger.LogError(ErrorMessages.Common.ModelValidationFailed);
                    TempData["ToastType"] = StaticData.ToastType.Warning;
                    TempData["ToastMessage"] = ErrorMessages.Common.ModelValidationFailed;
                    return RedirectToAction("Index", "User");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ErrorMessages.Common.ExceptionOccurred);
                _logger.LogTrace("[EXCEPTION] " + ex.Message);
                if (ex.InnerException != null) _logger.LogTrace("[INNER EXCEPTION] " + ex.InnerException.Message);

                TempData["ErrorViewModel"] = JsonConvert.SerializeObject(new ErrorViewModel { Message = ex.Message });
                return RedirectToAction("Error", "Home");
            }
            finally
            {
                _logger.LogDebug("OUT");
            }
        }

        [HttpGet]
        public async Task<IActionResult> DeleteUserAction([FromQuery] int userID)
        {
            _logger.LogDebug("IN");
            try
            {
                if (ModelState.IsValid)
                {
                    BaseResponse res = await _userClient.DeleteUserAsync(userID);
                    if (res.Code == ErrorCode.USER_S11014)
                    {
                        _logger.LogInformation("OK");
                        TempData["ToastType"] = StaticData.ToastType.Success;
                        TempData["ToastMessage"] = res.Message;
                        return RedirectToAction("Index", "User");
                    }
                    else
                    {
                        _logger.LogError(res.Message);
                        TempData["ToastType"] = StaticData.ToastType.Error;
                        TempData["ToastMessage"] = res.Message;
                        return RedirectToAction("Index", "User");
                    }
                }
                else
                {
                    _logger.LogError(ErrorMessages.Common.ModelValidationFailed);
                    TempData["ToastType"] = StaticData.ToastType.Warning;
                    TempData["ToastMessage"] = ErrorMessages.Common.ModelValidationFailed;
                    return RedirectToAction("Index", "User");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ErrorMessages.Common.ExceptionOccurred);
                _logger.LogTrace("[EXCEPTION] " + ex.Message);
                if (ex.InnerException != null) _logger.LogTrace("[INNER EXCEPTION] " + ex.InnerException.Message);

                TempData["ErrorViewModel"] = JsonConvert.SerializeObject(new ErrorViewModel { Message = ex.Message });
                return RedirectToAction("Error", "Home");
            }
            finally
            {
                _logger.LogDebug("OUT");
            }
        }

        [HttpGet]
        public async Task<IActionResult> ResetPassword([FromQuery] int userID)
        {
            _logger.LogDebug("IN");
            try
            {
                BaseResponse res = await _userClient.ResetPasswordAsync(userID);
                if (res.Code == ErrorCode.USER_S11020)
                {
                    _logger.LogInformation("OK");
                    TempData["ToastType"] = StaticData.ToastType.Success;
                    TempData["ToastMessage"] = res.Message;
                    return RedirectToAction("Index", "User");
                }
                else
                {
                    _logger.LogError(res.Message);
                    TempData["ToastType"] = StaticData.ToastType.Error;
                    TempData["ToastMessage"] = res.Message;
                    return RedirectToAction("Index", "User");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ErrorMessages.Common.ExceptionOccurred);
                _logger.LogTrace("[EXCEPTION] " + ex.Message);
                if (ex.InnerException != null) _logger.LogTrace("[INNER EXCEPTION] " + ex.InnerException.Message);

                TempData["ErrorViewModel"] = JsonConvert.SerializeObject(new ErrorViewModel { Message = ex.Message });
                return RedirectToAction("Error", "Home");
            }
            finally
            {
                _logger.LogDebug("OUT");
            }
        }
    }
}
