﻿using Fujin.DataContracts;
using Fujin.DataContracts.Base;
using Fujin.DataContracts.User;
using Fujin.WApp.Clients;
using Fujin.WApp.Common;
using Fujin.WApp.Models.Auth;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Fujin.WApp.Controllers
{
    public class AuthController : Controller
    {
        private readonly ILogger<AuthController> _logger;
        private readonly IAuthClient _authClient;

        public AuthController(ILogger<AuthController> logger, IAuthClient authClient)
        {
            _logger = logger;
            _authClient = authClient;
        }

        public IActionResult Login()
        {
            _logger.LogDebug("IN");
            _logger.LogDebug("OUT");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> LoginAction(LoginViewModel model)
        {
            _logger.LogDebug("IN");
            try
            {
                if (ModelState.IsValid)
                {
                    AuthRequest req = new()
                    {
                        Username = model.Username,
                        Password = BaseHelper.Base64Encode(model.Password)
                    };

                    AuthResponse res = await _authClient.Login(req);
                    if (res.Code == ErrorCode.USER_S11000)
                    {
                        var claims = new List<Claim>
                        {
                            new Claim(ClaimTypes.Name, res.UserInfo.user_id.ToString(CultureInfo.InvariantCulture))
                            //new Claim(ClaimTypes.Role, Role.getRole(res.UserInfo.FUserRoleID))
                        };
                        var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                        var authProperties = new AuthenticationProperties
                        {
                            IsPersistent = true,
                            ExpiresUtc = DateTime.UtcNow.AddHours(2)
                        };

                        await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(identity), authProperties);

                        _logger.LogInformation("OK");
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        _logger.LogError(res.Message);
                        TempData["Error"] = res.Message;
                        return RedirectToAction("Login", "Auth");
                    }
                }
                else
                {
                    _logger.LogError(ErrorMessages.Common.ModelValidationFailed);
                    TempData["Error"] = ErrorMessages.Common.ModelValidationFailed;
                    return RedirectToAction("Login", "Auth");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ErrorMessages.Common.ExceptionOccurred);
                _logger.LogTrace("[EXCEPTION] " + ex.Message);
                if (ex.InnerException != null) _logger.LogTrace("[INNER EXCEPTION] " + ex.InnerException.Message);
                return RedirectToAction("Login", "Auth");
            }
            finally
            {
                _logger.LogDebug("OUT");
            }
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> LogoutAction()
        {
            _logger.LogDebug("IN");
            try
            {
                BaseResponse res = await _authClient.Logout();

                if (res.Code == ErrorCode.USER_S11004)
                {
                    //sign out async
                    await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

                    _logger.LogInformation("OK");
                    return RedirectToAction("Login", "Auth");
                }
                else
                {
                    _logger.LogError(res.Message);
                    TempData["ToastType"] = StaticData.ToastType.Error;
                    TempData["ToastTitle"] = "Logout failed";
                    TempData["ToastMessage"] = res.Message;
                    return RedirectToAction("Index", "Home");
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ErrorMessages.Common.ExceptionOccurred);
                _logger.LogTrace("[EXCEPTION] " + ex.Message);
                if (ex.InnerException != null) _logger.LogTrace("[INNER EXCEPTION] " + ex.InnerException.Message);
                return RedirectToAction("Error", "Home");
            }
            finally
            {
                _logger.LogDebug("OUT");
            }
        }
    }
}
