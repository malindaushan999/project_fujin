﻿using Fujin.WApp.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fujin.WApp.Service
{
    public interface IHelperService
    {
        void UpdateSessionUser(UserViewModel user);
    }
}
