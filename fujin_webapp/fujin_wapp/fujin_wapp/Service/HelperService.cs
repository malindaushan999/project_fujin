﻿using Fujin.WApp.Common;
using Fujin.WApp.Models.User;
using Microsoft.AspNetCore.Http;

namespace Fujin.WApp.Service
{
    public class HelperService : IHelperService
    {
        /// <summary>
        /// Http Context Accessor
        /// </summary>
        private readonly IHttpContextAccessor _httpContextAccessor;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="httpContextAccessor">Http Context Accessor</param>
        public HelperService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Update UserInfo in session
        /// </summary>
        /// <param name="user">User object</param>
        public void UpdateSessionUser(UserViewModel user)
        {
            // Set session
            if (_httpContextAccessor.HttpContext is not null)
            {
                SessionStore sessionStore = _httpContextAccessor.HttpContext?.Session.GetObjectFromJson<SessionStore>("SessionDataStore");

                if (sessionStore is not null && sessionStore.User != null && user.UserID == sessionStore.User.UserID)
                {
                    sessionStore.User = user;
                    _httpContextAccessor.HttpContext.Session.SetObjectAsJson("SessionDataStore", sessionStore);
                }
            }
        }
    }
}
